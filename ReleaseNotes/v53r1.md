2021-10-04 Moore v53r1
===

This version uses
Phys [v33r2](../../../../Phys/-/tags/v33r2),
Allen [v1r6](../../../../Allen/-/tags/v1r6),
Rec [v33r2](../../../../Rec/-/tags/v33r2),
Lbcom [v33r3](../../../../Lbcom/-/tags/v33r3),
LHCb [v53r3](../../../../LHCb/-/tags/v53r3),
Gaudi [v36r1](../../../../Gaudi/-/tags/v36r1) and
LCG [100](http://lcginfo.cern.ch/release/100/) with ROOT 6.24.00.

This version is released on `master` branch.
Built relative to Moore [v53r0](/../../tags/v53r0), with the following changes:

### New features ~"new feature"

- ~hlt2 | Implement fastest reconstuction for selections, !984 (@sstahl)
- ~hlt2 | Add etac->hadron builders in bandq directory, and use thor functions to redefine bandq builders, !924 (@mengzhen) [`Rec#204]
- ~Configuration | Add Moore ConfigurableUser for compatibility with ProdConf, !1037 (@rmatev)


### Enhancements ~enhancement

- ~Configuration | Spruce reports and streaming, !998 (@nskidmor)
- ~Configuration | Add pass through options in Sprucing, !986 (@shunan) [lhcb-dpa/project#152,lhcb-dpa/project#22]


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Configuration | Follow changes in LHCb!3260, !1003 (@graven)
- ~Build | Disable very slow tests on clang-dbg to avoid wasting time, !1030 (@chasse)
- ~Build | Make_allen_tck prerequisite of mdf_read-decs_allen, !1013 (@dovombru) [#315]
- Adding missing script for running PVChecker on Allen output, !1019 (@adudziak)
- Update References for: Moore!984 based on lhcb-master-mr/2927, !1012 (@lhcbsoft)
- Update dump_for_standalone_allen.ref, !1002 (@maxime)


### Documentation ~Documentation

- Fix F.SUM(F.PT) in ThOr transition tutorial, !1007 (@dcervenk) [#312]
