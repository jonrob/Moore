2017-10-25 Moore v27r0
========================================

Release from master. The base of the 2018 production branch.
----------------------------------------
Based on Gaudi v29r0, LHCb v43r1, Lbcom v21r1, Rec v22r1, Phys v24r1, Hlt v27r0
This version is released on the master branch.

### Other
- Merge 2017-patches into master, !107 (@rmatev)  
  Ports MRs: !106 !104 !102 !100 !103
