###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options, run_reconstruction
from Moore.config import Reconstruction
from RecoConf.event_filters import require_gec
from RecoConf.reconstruction_objects import reconstruction as reco_objects_reconstruction
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction, make_fastest_reconstruction

from PyConf.Algorithms import TtracksMVAFilter


def standalone_mva_ttracks():
    ttracks = reco_objects_reconstruction(
    )['AllTrackHandles']['SeedDecloned']["v1"]
    prefilters = [require_gec()]
    filtered_tracks = TtracksMVAFilter(InputLocation=ttracks)
    data = [filtered_tracks]
    return Reconstruction('standalone_mva_ttracks', data, prefilters)


# Run with serial processing to allow muon ID
options.n_event_slots = 1
options.n_threads = 1
with reco_objects_reconstruction.bind(
        from_file=False), hlt2_reconstruction.bind(
            make_reconstruction=make_fastest_reconstruction):
    run_reconstruction(options, standalone_mva_ttracks)
