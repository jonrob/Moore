<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
    (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<!--
Test the monitoring output for non empty Histograms.
Runs over hlt2_2or3bodytopo_realtime.dst from hlt2_2or3bodytopo_realtime.py and requires the dumped TCK.
-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
<argument name="prerequisites"><set>
  <tuple><text>test_hlt2_2or3bodytopo_realtime</text><enumeral>PASS</enumeral></tuple>
</set></argument>
<argument name="program"><text>lbexec</text></argument>
<argument name="timeout"><integer>3000</integer></argument>
<argument name="args"><set>
  <text>Hlt2Conf.Sprucing_tests:spruce_example_realtime</text>
</set></argument>
<argument name="options_yaml_fn"><text>$HLT2CONFROOT/options/sprucing/lbexec_yamls/spruce_example_realtime.yaml</text></argument>
<argument name="extra_options_yaml"><text>histo_file : "monitoring_histos.root"</text></argument>
<argument name="use_temp_dir"><enumeral>true</enumeral></argument>
<argument name="validator"><text>

from Moore.qmtest.exclusions import remove_known_warnings
countErrorLines({"FATAL": 0, "ERROR": 0, "WARNING": 0},
                stdout=remove_known_warnings(stdout))

import os
from ROOT import TFile

histo_file="monitoring_histos.root"

if os.path.isfile(histo_file): print(f"Found file {histo_file}")
else: raise Exception(f"File: {histo_file} does not exist!")

inFile=TFile.Open(histo_file)

histo_b_mass_name='Spruce_Test_line/m'
histo_d_mass_name='Spruce_Test_line_Ds_Monitor/m'

histo_b_mass=inFile.Get(histo_b_mass_name)
histo_d_mass=inFile.Get(histo_d_mass_name)

if histo_d_mass.GetEntries() > 0: print("{0} entries in Ds mass histogram.".format(histo_d_mass.GetEntries()))
else: raise Exception("Could not find histogram {0}".format(histo_d_mass_name))

if histo_b_mass.GetEntries() > 0: print("{0} entries in B mass histogram.".format(histo_b_mass.GetEntries()))
else: raise Exception("Could not find histogram {0}".format(histo_b_mass_name))

</text></argument>
</extension>