###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test the pattern recognition and reconstruction of heavy flavour tracks 
together with a relevant line, as this reconstruction is only done in the line control flow

Runs over a B+->tau+nu MC sample that has been filtered on having at least one MCHit in the VELO of the B+ or tau+

"""
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.global_tools import stateProvider_with_simplified_geom, trackMasterExtrapolator_with_simplified_geom
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction, make_fastest_reconstruction

public_tools = [
    stateProvider_with_simplified_geom(),
    trackMasterExtrapolator_with_simplified_geom()
]

# input data
from Moore import options
options.set_input_and_conds_from_testfiledb(
    'Upgrade_Bu2TauNu_Tau2PiPiPiNu_HeavyFlavour_MCHit_Filtered')

from RecoConf.hlt1_muonid import make_muon_hits
make_muon_hits.global_bind(geometry_version=2)

from RecoConf.calorimeter_reconstruction import make_digits
make_digits.global_bind(calo_raw_bank=False)

# to run GaudiTupleAlg which is not thread safe
options.n_threads = 1

# output config
options.ntuple_file = 'output_btracking_checker.root'
with_dst_output = False

if with_dst_output:
    options.output_file = 'hlt2_btracking.dst'
    options.output_type = 'ROOT'
    options.output_manifest_file = "hlt2_btracking.tck.json"

# lines, sequences and control flow configuration
from Hlt2Conf.lines.semileptonic.hlt2_semileptonic import (
    hlt2_btotaunu_tautopipipinu_btracking_line, )


def all_lines_with_checker():
    setup = hlt2_btotaunu_tautopipipinu_btracking_line(include_mcchecker=True)
    return {'default': [setup['line']]}, setup['checker']


from PyConf.application import configure_input, configure
from PyConf.control_flow import CompositeNode, NodeLogic
from Moore import moore_control_flow


def run_moore_with_checker(options,
                           make_streams_with_checker,
                           public_tools=[],
                           allen_hlt1=False):
    config = configure_input(options)
    # Then create the data (and control) flow for all streams.
    streams, btracking_checker = make_streams_with_checker()
    # base Moore control flow
    moore_cf_node = moore_control_flow(options, streams, "hlt2", allen_hlt1)
    # add node on mc checking after lines and combine
    # as is has to run also if the line is not triggered
    algs = [btracking_checker]
    top_cf_node = CompositeNode(
        'with mc checker',
        combine_logic=NodeLogic.NONLAZY_AND,
        children=[moore_cf_node] + algs,
        force_order=True)
    config.update(configure(options, top_cf_node, public_tools=public_tools))
    return config


with reconstruction.bind(from_file=False),\
        hlt2_reconstruction.bind(make_reconstruction=make_fastest_reconstruction):
    run_moore_with_checker(options, all_lines_with_checker, public_tools)
