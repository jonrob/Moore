###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction
from RecoConf.hlt2_global_reco import make_fastest_reconstruction
import Moore
from Moore.streams_hlt2 import DETECTOR_RAW_BANK_TYPES
from PRConfig.bandwidth_helpers import FileNameHelper
import json
import re

from Hlt2Conf.lines import (
    b_to_open_charm,
    inclusive_detached_dilepton,
    rd,
    bandq,
    topological_b,
    pid,
    qee,
    charm,
    b_to_charmonia,
    semileptonic,
    monitoring,
    charmonium_to_dimuon,
    charmonium_to_dimuon_detached,
    bnoc,
    trackeff,
    ift,
)

MODULES = dict(
    b_to_open_charm=[b_to_open_charm],
    dilepton=[inclusive_detached_dilepton],
    rd=[rd],
    bandq=[bandq],
    topo_b=[topological_b],
    pid=[pid],
    qee=[qee],
    charm=[charm],
    b_to_charmonia=[b_to_charmonia],
    slepton=[semileptonic],
    monitoring=[monitoring],
    c_to_dimuon=[charmonium_to_dimuon, charmonium_to_dimuon_detached],
    bnoc=[bnoc],
    trackeff=[trackeff],
    ift=[ift])

fname_helper = FileNameHelper(process="hlt2")
fname_helper.make_tmp_dirs()
options.output_file = fname_helper.mdf_fname_for_Moore(stream_config="wg")
options.output_type = 'MDF'
options.output_manifest_file = fname_helper.tck(stream_config="wg")

# removed due to hlt1_filter_codes, which dont work on 2023 HLT1-filtered minbias
to_remove = [
    'Hlt2IFT_SMOG2GECPassthrough', 'Hlt2IFT_SMOG2LumiPassthrough',
    'Hlt2IFT_SMOG2MBPassthrough', 'Hlt2IFT_Femtoscopy_InclLambdaLL',
    'Hlt2IFT_Femtoscopy_InclXiLLL', 'Hlt2IFT_Femtoscopy_InclOmegaLLL',
    'Hlt2IFT_Femtoscopy_LambdaP', 'Hlt2IFT_Femtoscopy_LambdaP_lowK',
    'Hlt2IFT_Femtoscopy_LambdaLambda', 'Hlt2IFT_Femtoscopy_LambdaLambda_lowK',
    'Hlt2IFT_Femtoscopy_XiP', 'Hlt2IFT_Femtoscopy_XiP_lowK',
    'Hlt2IFT_Femtoscopy_XiLambda', 'Hlt2IFT_Femtoscopy_XiLambda_lowK',
    'Hlt2IFT_Femtoscopy_XiXi', 'Hlt2IFT_Femtoscopy_OmegaP',
    'Hlt2IFT_Femtoscopy_OmegaP', 'Hlt2IFT_Femtoscopy_OmegaP_lowK',
    'Hlt2IFT_Femtoscopy_OmegaLambda', 'Hlt2IFT_Femtoscopy_OmegaXi',
    'Hlt2IFT_Femtoscopy_OmegaOmega', 'Hlt2QEE_DiElectronPrompt_PersistPhotons',
    'Hlt2QEE_DiElectronPrompt_PersistPhotons_FULL',
    'Hlt2QEE_DiElectronDisplaced_PersistPhotons',
    'Hlt2QEE_DiElectronDisplaced_PersistPhotons_FULL',
    'Hlt2QEE_DiElectronPrompt_PersistPhotonsSS',
    'Hlt2QEE_DiElectronDisplaced_PersistPhotonsSS'
]


def make_module_lines(mod):

    builders = []
    try:
        lines = mod.all_lines
        for name, builder in lines.items():
            if all([
                    re.search(regex_pattern, name) is None
                    for regex_pattern in to_remove
            ]):
                builders.append(builder())
            else:
                print(f"Manually removed {name} line.")
    except AttributeError:
        raise AttributeError(
            f'line module {mod.__name__} does not define mandatory `all_lines`'
        )

    return builders


def make_module_streams():

    linedict = {mod_name: [] for mod_name in MODULES.keys()}
    for mod_name, module_list in MODULES.items():
        for module in module_list:
            builders = make_module_lines(module)
            if builders:
                linedict[mod_name] += builders

    # Write out stream configuration to JSON file for use later in the test
    with open(fname_helper.stream_config_json_path(stream_config="wg"),
              'w') as f:
        json.dump({k: [line.name for line in v]
                   for k, v in linedict.items()}, f)

    return linedict


banks_default = []
banks_turcal = DETECTOR_RAW_BANK_TYPES

Moore.streams_hlt2.DETECTOR_RAW_BANK_TYPES_PER_STREAM = {
    'b_to_open_charm': banks_default,
    'dilepton': banks_default,
    'rd': banks_default,
    'bandq': banks_default,
    'topo_b': banks_default,
    'pid': banks_turcal,
    'qee': banks_default,
    'charm': banks_default,
    'b_to_charmonia': banks_default,
    'slepton': banks_default,
    'monitoring': banks_default,
    'c_to_dimuon': banks_default,
    'bnoc': banks_default,
    'trackeff': banks_turcal,
    'ift': banks_turcal,
}

Moore.streams_hlt2.stream_bits = {
    'b_to_open_charm': 66,
    'dilepton': 67,
    'rd': 68,
    'bandq': 69,
    'topo_b': 70,
    'pid': 71,
    'qee': 72,
    'charm': 73,
    'b_to_charmonia': 74,
    'slepton': 75,
    'monitoring': 76,
    'c_to_dimuon': 77,
    'bnoc': 78,
    'trackeff': 79,
    'ift': 80,
}

public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False),\
    hlt2_reconstruction.bind(make_reconstruction=make_fastest_reconstruction):
    config = run_moore(
        options, make_module_streams, public_tools, exclude_incompatible=True)
