###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Moore import options, run_moore
from RecoConf.global_tools import stateProvider_with_simplified_geom
from RecoConf.reconstruction_objects import reconstruction
from RecoConf.hlt2_global_reco import reconstruction as hlt2_reconstruction
from RecoConf.hlt2_global_reco import make_fastest_reconstruction
from Hlt2Conf.settings.hlt2_pp_commissioning import _make_streams
import re
import json
from PRConfig.bandwidth_helpers import FileNameHelper

fname_helper = FileNameHelper(process="hlt2")
fname_helper.make_tmp_dirs()
options.output_file = fname_helper.mdf_fname_for_Moore(
    stream_config="production")
options.output_type = 'MDF'
options.output_manifest_file = fname_helper.tck(stream_config="production")

to_remove = [
    'Hlt2Lumi',
    'Hlt2Commissioning_KsToPimPip_LL_Hlt1KsTOS',
    'Hlt2Commissioning_PhiToKmKp',
    'Hlt2Commissioning_L0ToPpPim_LL_Hlt1L02PPiTOS',
    'Hlt2Commissioning_D0ToKmPip_Hlt1D2KPiTOS',
    'Hlt2Commissioning_DpToKmPipPip_Hlt1TrackMVATOS',
]

# removed due to hlt1_filter_codes, which dont work on 2023 HLT1-filtered minbias
to_remove += [
    'Hlt2IFT_SMOG2GECPassthrough', 'Hlt2IFT_SMOG2LumiPassthrough',
    'Hlt2IFT_SMOG2MBPassthrough', 'Hlt2IFT_Femtoscopy_InclLambdaLL',
    'Hlt2IFT_Femtoscopy_InclXiLLL', 'Hlt2IFT_Femtoscopy_InclOmegaLLL',
    'Hlt2IFT_Femtoscopy_LambdaP', 'Hlt2IFT_Femtoscopy_LambdaP_lowK',
    'Hlt2IFT_Femtoscopy_LambdaLambda', 'Hlt2IFT_Femtoscopy_LambdaLambda_lowK',
    'Hlt2IFT_Femtoscopy_XiP', 'Hlt2IFT_Femtoscopy_XiP_lowK',
    'Hlt2IFT_Femtoscopy_XiLambda', 'Hlt2IFT_Femtoscopy_XiLambda_lowK',
    'Hlt2IFT_Femtoscopy_XiXi', 'Hlt2IFT_Femtoscopy_OmegaP',
    'Hlt2IFT_Femtoscopy_OmegaP', 'Hlt2IFT_Femtoscopy_OmegaP_lowK',
    'Hlt2IFT_Femtoscopy_OmegaLambda', 'Hlt2IFT_Femtoscopy_OmegaXi',
    'Hlt2IFT_Femtoscopy_OmegaOmega', 'Hlt2QEE_DiElectronPrompt_PersistPhotons',
    'Hlt2QEE_DiElectronPrompt_PersistPhotons_FULL',
    'Hlt2QEE_DiElectronDisplaced_PersistPhotons',
    'Hlt2QEE_DiElectronDisplaced_PersistPhotons_FULL',
    'Hlt2QEE_DiElectronPrompt_PersistPhotonsSS',
    'Hlt2QEE_DiElectronDisplaced_PersistPhotonsSS'
]


def remove_lines(linelist, regex_pattern):
    return [
        line for line in linelist
        if re.search(regex_pattern, line.name) is None
    ]


def make_module_streams():
    '''
        Picks up production stream settings as in hlt2_pp_commissioning
        Removes lumi, no_bias, turboraw and passthrough streams
    '''

    CONFIG = _make_streams()

    linedict = {
        'turbo': CONFIG['turbo'],
        'full': CONFIG['full'],
        'turcal': CONFIG['turcal']
    }

    for line in to_remove:
        for stream, lines in linedict.items():
            filtered = remove_lines(lines, line)
            if len(filtered) != len(lines):
                print("Manually removed {} line from {} stream".format(
                    line, stream.upper()))
                linedict.update({stream: filtered})  # Modify dictionary

    # Write out stream configuration to JSON file for use later in the test
    with open(
            fname_helper.stream_config_json_path(stream_config="production"),
            'w') as f:
        json.dump({k: [line.name for line in v]
                   for k, v in linedict.items()}, f)

    return linedict


public_tools = [stateProvider_with_simplified_geom()]
with reconstruction.bind(from_file=False),\
    hlt2_reconstruction.bind(make_reconstruction=make_fastest_reconstruction):
    config = run_moore(
        options, make_module_streams, public_tools, exclude_incompatible=True)
