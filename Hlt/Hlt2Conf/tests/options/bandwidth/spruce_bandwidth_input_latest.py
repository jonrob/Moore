###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Configure input files for the Sprucing bandwidth test on 'latest'

This is needed for the qmtest in Moore.
For the test in LHCbPR, the input files are configured by
$PRCONFIGROOT/python/MooreTests/run_bandwidth_test_jobs.py

If updating, please also inform one of: @lugrazet @rjhunter @shunan
'''
from Moore import options

options.dddb_tag = "dddb-20171010"
options.conddb_tag = "sim-20170301-vc-md100"
options.simulation = True
options.input_type = 'MDF'
options.evt_max = 100
options.n_threads = 1
options.data_type = 'Upgrade'
options.input_raw_format = 0.5
options.input_files = [
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/storage/lhcbpr/www/UpgradeRateTest/current_hlt2_output/hlt2_bw_testing__production__full.mdf'
]
options.input_manifest_file = 'root://eoslhcb.cern.ch//eos/lhcb/storage/lhcbpr/www/UpgradeRateTest/current_hlt2_output/hlt2_bw_testing__production.tck.json'

from RecoConf.hlt1_muonid import make_muon_hits
make_muon_hits.global_bind(geometry_version=2)
