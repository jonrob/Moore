###############################################################################
# (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Test the persistency of SOA objects
- Tracks
"""

from Configurables import (ApplicationMgr, LHCbApp, HltPackedBufferWriter,
                           SOATrackPacker, ProduceSOATracks,
                           UniqueIDGeneratorAlg)

from PyConf.application import configured_ann_svc

from PyConf.filecontent_metadata import register_encoding_dictionary
from PRConfig import TestFileDB

TES = "/Event/Rec"
locations = []
locations += [TES + "/Tracks"]

encoding_key = int(
    register_encoding_dictionary("PackedObjectLocations", sorted(locations)),
    16)

uniqueid_gen = UniqueIDGeneratorAlg(Output=TES + "/UniqueId")

# Tracks
track_producer = ProduceSOATracks(
    InputUniqueIDGenerator=TES + "/UniqueId", Output="/Event/Rec/Tracks")
track_packer = SOATrackPacker(
    InputName=[TES + "/Tracks"],
    OutputName=TES + "/Buffers/Tracks",
    EnableCheck=True,
    OutputLevel=2,
    EncodingKey=encoding_key)

# Writer
bank_writer = HltPackedBufferWriter(
    PackedContainers=[TES + "/Buffers/Tracks"],
    OutputView="/Event/RawBanks/DstData",
    OutputLevel=2,
    SourceID="Hlt2")

ApplicationMgr(
    TopAlg=[uniqueid_gen, track_producer, track_packer, bank_writer],
    ExtSvc=[configured_ann_svc(name='HltANNSvc')])

app = LHCbApp(
    DataType="Upgrade",
    Simulation=True,
)

app.EvtMax = 5
# Pick a file that has the reconstruction available
# Events are not really used, this is a dummy setup
f = TestFileDB.test_file_db["upgrade_sim10_Up08_Digi15_D2KsPi_MU"]

f.setqualifiers(configurable=app)
f.run(configurable=app, withDB=True)
