###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from __future__ import absolute_import, division, print_function

from GaudiKernel.SystemOfUnits import GeV, MeV

from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.algorithms_thor import (ParticleCombiner, ParticleFilter)
import Functors as F
from Functors.math import in_range
from PyConf import configurable
from Hlt2Conf.standard_particles import make_ismuon_long_muon

# MASS WINDOWS
_MASSWINDOW_JPSI = [2900. * MeV, 3300. * MeV]
_MASSWINDOW_PSI2S = [3500.0 * MeV, 3900.0 * MeV]
_MASSMINDOW_UPS = [8000.0 * MeV, 12000.0 * MeV]
_MASSWINDOW_Z = [60. * GeV, 120.0 * GeV]
_MASSWINDOW_DY1 = [1.2 * GeV, 3.0 * GeV]

# PID CUT FOR SPRUCE AND HLT2 (HLT2 SET TO NONE FOR NOW)

_SPRUCE_PIDmu_JPSI = None  #"PIDmu > 1"
_SPRUCE_PIDmu_PSI2S = None  #"PIDmu > 1"
_SPRUCE_PIDmu_UPSILON = None  #"PIDmu > 1"
_SPRUCE_PIDmu_Z = None  #"PIDmu > 1"
_SPRUCE_PIDmu_DY = None  #"PIDmu > 1"

_HLT2_PIDmu_JPSI = None
_HLT2_PIDmu_PSI2S = None
_HLT2_PIDmu_UPSILON = None
_HLT2_PIDmu_Z = None


def make_muons(pid=None,
               trchi2_max=3.,
               trghostprob_max=1,
               isMuon=False,
               pt_min=700 * MeV):
    code = F.require_all(F.GHOSTPROB < trghostprob_max, F.PT > pt_min,
                         F.CHI2DOF < trchi2_max)
    if pid is not None:
        pid_cuts = F.PID_MU > pid
        code &= (pid_cuts)
    if isMuon is not None:
        ismuon_cuts = F.ISMUON
        code &= ismuon_cuts
    return ParticleFilter(make_ismuon_long_muon(), F.FILTER(code))


## constructor for the different Dimuon lines
@configurable
def make_dimuon(particle1,
                particle2,
                descriptors,
                pvs,
                pt_mom_min=0 * MeV,
                vchi2pdof_max=25.):

    combination_code = F.require_all(F.PT > pt_mom_min)
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max)

    return ParticleCombiner([particle1, particle2],
                            DecayDescriptor=descriptors,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code)


@configurable
def make_jpsi(process="hlt2",
              mmin=_MASSWINDOW_JPSI[0],
              mmax=_MASSWINDOW_JPSI[1],
              pt_mom_min=0 * MeV,
              vchi2pdof_max=25.,
              pid_cut=-99999,
              trchi2_max=3.,
              trghostprob_max=1,
              isMuon=True,
              pt_min=700 * MeV):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'
    pvs = make_pvs()
    code = F.require_all(in_range(mmin, F.MASS, mmax))
    if process == 'spruce':
        muons = make_muons(
            pid=pid_cut,
            trchi2_max=trchi2_max,
            trghostprob_max=trghostprob_max,
            isMuon=isMuon,
            pt_min=pt_min)
        dimuon = make_dimuon(muons, muons, 'J/psi(1S) -> mu- mu+', pvs=pvs)
    if process == 'hlt2':
        muons = make_muons(
            pid=pid_cut,
            trchi2_max=trchi2_max,
            trghostprob_max=trghostprob_max,
            isMuon=isMuon,
            pt_min=pt_min)
        dimuon = make_dimuon(muons, muons, 'J/psi(1S) -> mu- mu+', pvs=pvs)

    return ParticleFilter(dimuon, F.FILTER(code))


@configurable
def make_psi2s(process="hlt2",
               mmin=_MASSWINDOW_PSI2S[0],
               mmax=_MASSWINDOW_PSI2S[1],
               pt_mom_min=0 * MeV,
               vchi2pdof_max=25.,
               pid_cut=None,
               trchi2_max=3.,
               trghostprob_max=1,
               isMuon=True,
               pt_min=700 * MeV):
    assert process in ['hlt2', 'spruce'
                       ], 'Line must be defined as Hlt2 or Sprucing line!'

    code = F.require_all(in_range(mmin, F.MASS, mmax))
    pvs = make_pvs()
    if process == 'spruce':
        muons = make_muons(
            pid=pid_cut,
            trchi2_max=trchi2_max,
            trghostprob_max=trghostprob_max,
            isMuon=isMuon,
            pt_min=pt_min)
        dimuon = make_dimuon(muons, muons, 'psi(2S) -> mu- mu+', pvs=pvs)
    if process == 'hlt2':
        muons = make_muons(
            pid=pid_cut,
            trchi2_max=trchi2_max,
            trghostprob_max=trghostprob_max,
            isMuon=isMuon,
            pt_min=pt_min)
        dimuon = make_dimuon(muons, muons, 'psi(2S) -> mu- mu+', pvs=pvs)

    return ParticleFilter(dimuon, F.FILTER(code))


@configurable
def make_upsilon(process="hlt2",
                 mmin=_MASSMINDOW_UPS[0],
                 mmax=_MASSMINDOW_UPS[1],
                 pt_mom_min=0 * MeV,
                 vchi2pdof_max=25.,
                 pid_cut=None,
                 trchi2_max=3.,
                 trghostprob_max=1,
                 isMuon=True,
                 pt_min=700 * MeV):

    code = F.require_all(in_range(mmin, F.MASS, mmax))
    pvs = make_pvs()
    if process == 'spruce':
        muons = make_muons(
            pid=pid_cut,
            trchi2_max=trchi2_max,
            trghostprob_max=trghostprob_max,
            isMuon=isMuon,
            pt_min=pt_min)
        dimuon = make_dimuon(muons, muons, 'Upsilon(1S) -> mu- mu+', pvs,
                             pt_mom_min, vchi2pdof_max)
    if process == 'hlt2':
        muons = make_muons(
            pid=pid_cut,
            trchi2_max=trchi2_max,
            trghostprob_max=trghostprob_max,
            isMuon=isMuon,
            pt_min=pt_min)
        dimuon = make_dimuon(muons, muons, 'Upsilon(1S) -> mu- mu+', pvs,
                             pt_mom_min, vchi2pdof_max)

    return ParticleFilter(dimuon, F.FILTER(code))


@configurable
def make_Zboson(process="hlt2",
                mmin=_MASSWINDOW_Z[0],
                mmax=_MASSWINDOW_Z[1],
                pt_mom_min=0 * MeV,
                vchi2pdof_max=25.,
                pid_cut=None,
                trchi2_max=3.,
                trghostprob_max=1,
                isMuon=True,
                pt_min=700 * MeV):

    code = F.require_all(in_range(mmin, F.MASS, mmax))
    pvs = make_pvs()
    if process == 'spruce':
        muons = make_muons(
            pid=pid_cut,
            trchi2_max=trchi2_max,
            trghostprob_max=trghostprob_max,
            isMuon=isMuon,
            pt_min=pt_min)
        dimuon = make_dimuon(muons, muons, 'Z0 -> mu- mu+', pvs, pt_mom_min,
                             vchi2pdof_max)
    if process == 'hlt2':
        muons = make_muons(
            pid=pid_cut,
            trchi2_max=trchi2_max,
            trghostprob_max=trghostprob_max,
            isMuon=isMuon,
            pt_min=pt_min)
        dimuon = make_dimuon(muons, muons, 'Z0 -> mu- mu+', pvs, pt_mom_min,
                             vchi2pdof_max)

    return ParticleFilter(dimuon, F.FILTER(code))


@configurable
def make_DY(process="hlt2",
            mmin=_MASSWINDOW_DY1[0],
            mmax=_MASSWINDOW_DY1[1],
            pt_mom_min=0 * MeV,
            vchi2pdof_max=25.,
            pid_cut=None,
            trchi2_max=3.,
            trghostprob_max=1,
            isMuon=True,
            pt_min=700 * MeV):

    code = F.require_all(in_range(mmin, F.MASS, mmax))
    pvs = make_pvs()
    if process == 'spruce':
        muons = make_muons(
            pid=pid_cut,
            trchi2_max=trchi2_max,
            trghostprob_max=trghostprob_max,
            isMuon=isMuon,
            pt_min=pt_min)
        dimuon = make_dimuon(muons, muons, 'Z0 -> mu- mu+', pvs, pt_mom_min,
                             vchi2pdof_max)
    if process == 'hlt2':
        muons = make_muons(
            pid=pid_cut,
            trchi2_max=trchi2_max,
            trghostprob_max=trghostprob_max,
            isMuon=isMuon,
            pt_min=pt_min)
        dimuon = make_dimuon(muons, muons, 'Z0 -> mu- mu+', pvs, pt_mom_min,
                             vchi2pdof_max)

    return ParticleFilter(dimuon, F.FILTER(code))


@configurable
def make_DYLS(process="hlt2",
              massWind_DY=_MASSWINDOW_DY1,
              pt_mom_min=0 * MeV,
              vchi2pdof_max=25.,
              pid_cut=None,
              trchi2_max=3.,
              trghostprob_max=1,
              isMuon=True,
              pt_min=700 * MeV):

    mmax = massWind_DY[1]
    mmin = massWind_DY[0]
    code = F.require_all(in_range(mmin, F.MASS, mmax))
    pvs = make_pvs()

    if process == 'spruce':
        muons = make_muons(
            pid=pid_cut,
            isMuon=isMuon,
            trchi2_max=trchi2_max,
            trghostprob_max=trghostprob_max,
            pt_min=pt_min)
        dimuon = make_dimuon(muons, muons, '[Z0 -> mu+ mu+]cc', pvs,
                             pt_mom_min, vchi2pdof_max)
    if process == 'hlt2':
        muons = make_muons(
            pid=pid_cut,
            isMuon=isMuon,
            trchi2_max=trchi2_max,
            trghostprob_max=trghostprob_max,
            pt_min=pt_min)
        dimuon = make_dimuon(muons, muons, '[Z0 -> mu+ mu+]cc', pvs,
                             pt_mom_min, vchi2pdof_max)
    return ParticleFilter(dimuon, F.FILTER(code))
