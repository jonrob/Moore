# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Submodule that defines all the ift HLT2 lines
"""

from . import hlt2_iftPbPb

all_lines = {}
all_lines.update(hlt2_iftPbPb.all_lines)

from . import spruce_iftPbPb

sprucing_lines = {}
sprucing_lines.update(spruce_iftPbPb.sprucing_lines)
