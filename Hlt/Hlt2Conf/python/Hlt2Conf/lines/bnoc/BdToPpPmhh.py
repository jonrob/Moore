###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of BNOC BdToPpPmhh lines
"""

from GaudiKernel.SystemOfUnits import MeV
from Hlt2Conf.lines.bnoc.builders.b_builder import make_B2PpPmhh
from Hlt2Conf.lines.bnoc.utils import check_process
from Hlt2Conf.lines.bnoc.builders.basic_builder import make_pions, make_tight_protons


@check_process
def make_BdToPpPmPipPim_NoPID(process):
    pions = make_pions(pi_pidk_max=None, p_min=1500 * MeV, pt_min=500 * MeV)
    protons = make_tight_protons(mipchi2_min=4., p_pidp_min=-3)
    Bd0 = make_B2PpPmhh(
        particles=[protons, protons, pions, pions],
        descriptor="B0 -> p+ p~- pi+ pi-")
    return Bd0
