###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of BNOC BToLambdahhh lines, with LL and DD Lambda
* Use standard particle maker
* Need to retune NoPID lines
"""

from PyConf import configurable
from GaudiKernel.SystemOfUnits import MeV, mm
from Hlt2Conf.lines.bnoc.utils import check_process
from Hlt2Conf.lines.bnoc.builders import basic_builder
from Hlt2Conf.lines.bnoc.builders.b_builder import make_b2x

# aprox masses of particles: lower than true value because of detector resolution
mL0 = 1110 * MeV
mp = 930 * MeV
mpi = 120 * MeV
mK = 480 * MeV
mMu = 100 * MeV
all_lines = {}

Bu_kwargs = {
    "am_min": 4900 * MeV,
    "am_max": 5600 * MeV,
    "MassWindow": False,
    "am12_max": 4500. * MeV,
    "am123_max": 5000. * MeV,
    "adoca_max": .5 * mm,
    "dira_min": 0.9995,
    "motherpt_min": 2000 * MeV,
    "achi2doca_max": 18.0,
    "bpvipchi2_max": 11,
    "mipchi2_max": 12,
    "vtxchi2pdof_max": 12,
}

Bds_kwargs = {
    "am_min": 4600 * MeV,
    "am_max": 5600 * MeV,
    "MassWindow": False,
    "am12_max": 5000. * MeV,
    "am123_max": 5200. * MeV,
    "adoca_max": .5 * mm,
    "dira_min": 0.9995,
    "motherpt_min": 1500 * MeV,
    "achi2doca_max": 25.0,
    "bpvipchi2_max": 15,
    "vtxchi2pdof_max": 15,
}

###############
## L0barphh  ##
###############


@configurable
def make_BuToL0barPpPpPm_LL(process):

    lambdas = basic_builder.make_veryloose_lambda_LL()
    protons = basic_builder.make_tight_protons(mipchi2_min=4.)
    line_alg = make_b2x(
        particles=[lambdas, protons, protons, protons],
        descriptor="[B+ -> Lambda~0 p+ p+  p~-]cc",
        **Bu_kwargs,
        am12_min=mL0 + mp,
        am123_min=mL0 + mp + mp)
    return [lambdas, line_alg]


@configurable
def make_BuToL0barPpPpPm_DD(process):

    lambdas = basic_builder.make_loose_lambda_DD()
    protons = basic_builder.make_tight_protons(mipchi2_min=4.)

    line_alg = make_b2x(
        particles=[lambdas, protons, protons, protons],
        descriptor="[B+ -> Lambda~0 p+ p+ p~-]cc",
        **Bu_kwargs,
        am12_min=mL0 + mp,
        am123_min=mL0 + mp + mp)
    return [lambdas, line_alg]


@configurable
def make_BuToL0barPpPipPim_NoPID_LL(process):

    lambdas = basic_builder.make_veryloose_lambda_LL()
    protons = basic_builder.make_tight_protons(mipchi2_min=4.)
    pions = basic_builder.make_pions(
        pi_pidk_max=None, p_min=1500 * MeV, pt_min=500 * MeV)
    line_alg = make_b2x(
        particles=[lambdas, protons, pions, pions],
        descriptor="[B+ -> Lambda~0 p+ pi- pi+ ]cc",
        **Bu_kwargs,
        am12_min=mL0 + mp,
        am123_min=mL0 + mp + mpi)
    return [lambdas, line_alg]


@configurable
def make_BuToL0barPpPipPim_NoPID_DD(process):

    lambdas = basic_builder.make_loose_lambda_DD()
    protons = basic_builder.make_tight_protons(mipchi2_min=4.)
    pions = basic_builder.make_pions(
        pi_pidk_max=None, p_min=1500 * MeV, pt_min=500 * MeV)
    line_alg = make_b2x(
        particles=[lambdas, protons, pions, pions],
        descriptor="[B+ -> Lambda~0 p+ pi- pi+]cc",
        **Bu_kwargs,
        am12_min=mL0 + mp,
        am123_min=mL0 + mp + mpi)
    return [lambdas, line_alg]


@configurable
def make_BuToL0PmPipPip_NoPID_LL(process):

    lambdas = basic_builder.make_veryloose_lambda_LL()
    protons = basic_builder.make_tight_protons(mipchi2_min=4.)

    pions = basic_builder.make_pions(
        pi_pidk_max=None, p_min=1500 * MeV, pt_min=500 * MeV)
    line_alg = make_b2x(
        particles=[lambdas, protons, pions, pions],
        descriptor="[B+ -> Lambda0 p~- pi+ pi+]cc",
        **Bu_kwargs,
        am12_min=mL0 + mp,
        am123_min=mL0 + mp + mpi)
    return [lambdas, line_alg]


@configurable
def make_BuToL0PmPipPip_NoPID_DD(process):

    lambdas = basic_builder.make_loose_lambda_DD()
    protons = basic_builder.make_tight_protons(mipchi2_min=4.)

    pions = basic_builder.make_pions(
        pi_pidk_max=None, p_min=1500 * MeV, pt_min=500 * MeV)
    line_alg = make_b2x(
        particles=[lambdas, protons, pions, pions],
        descriptor="[B+ -> Lambda0 p~- pi+ pi+]cc",
        **Bu_kwargs,
        am12_min=mL0 + mp,
        am123_min=mL0 + mp + mpi)
    return [lambdas, line_alg]


###############
## L0L0barhh ##
###############


@check_process
@configurable
def make_BdsToL0L0barPpPm_LLLL(process):

    lambdas = basic_builder.make_veryloose_lambda_LL()
    protons = basic_builder.make_tight_protons(mipchi2_min=4.)

    line_alg = make_b2x(
        particles=[lambdas, lambdas, protons, protons],
        descriptor="B0 -> Lambda0 Lambda~0 p~- p+",
        **Bds_kwargs,
        am12_min=2 * mL0,
        am123_min=2 * mL0 + mp)
    return [lambdas, line_alg]


@check_process
@configurable
def make_BdsToL0L0barPpPm_DDDD(process):

    lambdas = basic_builder.make_loose_lambda_DD()
    protons = basic_builder.make_tight_protons(mipchi2_min=4.)

    line_alg = make_b2x(
        particles=[lambdas, lambdas, protons, protons],
        descriptor="B0 -> Lambda0 Lambda~0 p~- p+",
        **Bds_kwargs,
        am12_min=2 * mL0,
        am123_min=2 * mL0 + mp)
    return [lambdas, line_alg]


@check_process
@configurable
def make_BdsToL0L0barPpPm_LLDD(process):

    lambdasll = basic_builder.make_veryloose_lambda_LL()
    lambdasdd = basic_builder.make_loose_lambda_DD()
    protons = basic_builder.make_tight_protons(mipchi2_min=4.)

    line_alg = make_b2x(
        particles=[lambdasll, lambdasdd, protons, protons],
        descriptor="[B0 -> Lambda0 Lambda~0 p~- p+]cc",
        **Bds_kwargs,
        am12_min=2 * mL0,
        am123_min=2 * mL0 + mp)
    return [lambdasll, lambdasdd, line_alg]


@check_process
@configurable
def make_BdsToL0L0barPipPim_NoPID_LLLL(process):

    lambdas = basic_builder.make_veryloose_lambda_LL()
    pions = basic_builder.make_pions(
        pi_pidk_max=None, p_min=1500 * MeV, pt_min=300 * MeV)

    line_alg = make_b2x(
        particles=[lambdas, lambdas, pions, pions],
        descriptor="B0 -> Lambda0 Lambda~0 pi+ pi-",
        **Bds_kwargs,
        am12_min=2 * mL0,
        am123_min=2 * mL0 + mpi)
    return [lambdas, line_alg]


@check_process
@configurable
def make_BdsToL0L0barPipPim_NoPID_DDDD(process):

    lambdas = basic_builder.make_loose_lambda_DD()
    pions = basic_builder.make_pions(
        pi_pidk_max=None, p_min=1500 * MeV, pt_min=300 * MeV)

    line_alg = make_b2x(
        particles=[lambdas, lambdas, pions, pions],
        descriptor="B0 -> Lambda0 Lambda~0 pi+ pi-",
        **Bds_kwargs,
        am12_min=2 * mL0,
        am123_min=2 * mL0 + mpi)
    return [lambdas, line_alg]


@check_process
@configurable
def make_BdsToL0L0barPipPim_NoPID_LLDD(process):

    lambdasll = basic_builder.make_veryloose_lambda_LL()
    lambdasdd = basic_builder.make_loose_lambda_DD()
    pions = basic_builder.make_pions(
        pi_pidk_max=None, p_min=1500 * MeV, pt_min=300 * MeV)

    line_alg = make_b2x(
        particles=[lambdasll, lambdasdd, pions, pions],
        descriptor="[B0 -> Lambda0 Lambda~0 pi+ pi-]cc",
        **Bds_kwargs,
        am12_min=2 * mL0,
        am123_min=2 * mL0 + mpi)
    return [lambdasll, lambdasdd, line_alg]
