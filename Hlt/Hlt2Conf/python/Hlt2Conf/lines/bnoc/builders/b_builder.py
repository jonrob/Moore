###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Builds BNOC B decays, and defines the common default cuts applied to the B2X combinations
"""

from GaudiKernel.SystemOfUnits import GeV, MeV, mm, picosecond

from PyConf import configurable

from RecoConf.reconstruction_objects import make_pvs

import Functors as F
from Functors.math import in_range
from Hlt2Conf.algorithms_thor import ParticleCombiner
from Functors import require_all

##############################################
# For BdToPpPmhh lines                       #
##############################################


@configurable
def make_B2PpPmhh(particles,
                  descriptor,
                  name='BNOC_B2PpPmhhCombiner_{hash}',
                  am_max_ppbar=5000.0 * MeV,
                  adoca_chi2_ppbar=20.0,
                  asum_PT_ppbar=750.0 * MeV,
                  asum_P_ppbar=7000.0 * MeV,
                  am_max_ppbarK=5600.0 * MeV,
                  adoca_chi2_ppbarK=20.0,
                  am_min_ppbarKpi=5000 * MeV,
                  am_max_ppbarKpi=5700 * MeV,
                  adoca_chi2_ppbarKpi=20.0,
                  amaxdoca4h=0.25 * mm,
                  comb_PTSUM_min=3000.0 * MeV,
                  B_dira_min=0.9999,
                  B_vtx_CHI2_max=25.0,
                  B_PT_min=1000.0 * MeV,
                  B_minip=0.2 * mm):

    combination12_code = F.require_all(
        F.MASS < am_max_ppbar,
        F.SUM(F.PT) > asum_PT_ppbar,
        F.SUM(F.P) > asum_P_ppbar,
        F.MAXSDOCACHI2CUT(adoca_chi2_ppbar))  #cuts on the ppbar combination

    combination123_code = F.require_all(
        F.MASS < am_max_ppbarK,
        F.MAXSDOCACHI2CUT(adoca_chi2_ppbarK))  #cuts on the ppbarK combination

    combination_code = F.require_all(
        in_range(0.9 * am_min_ppbarKpi, F.MASS, 1.1 * am_max_ppbarKpi),
        F.MAXSDOCACHI2CUT(
            adoca_chi2_ppbarKpi))  #cuts on the ppbarKpi combination

    pvs = make_pvs()

    vertex_code = F.require_all(
        in_range(am_min_ppbarKpi, F.MASS, am_max_ppbarKpi),
        F.BPVDIRA(pvs) > B_dira_min, F.CHI2DOF < B_vtx_CHI2_max,
        F.SUM(F.PT) > comb_PTSUM_min, F.PT > B_PT_min,
        F.MINIPCHI2(pvs) < B_minip)  #cuts on the B0

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=combination12_code,
        Combination123Cut=combination123_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


##############################################
# For BdsTohhhh and BuToKShhh lines          #
##############################################


@configurable
def make_b2x(
        particles,
        descriptor,
        name='BNOC_B2XCombiner_{hash}',
        am_min=4500 * MeV,  #Beware TightCut!
        am_max=6300 * MeV,
        MassWindow=False,  #VV-AS inclusive mass window
        am123_max=1900. * MeV,
        am12_max=1900. * MeV,  # include D0
        am123_min=0. * MeV,  #should be at production threshold
        am12_min=0. * MeV,
        mothermass_min=4800 * MeV,
        mothermass_max=6100 * MeV,
        asumpt_min=1.5 * GeV,  #5 GeV???
        motherpt_min=250. * MeV,  # 1500 * MeV Run12
        achi2doca_max=30.,
        bpvipchi2_max=25.,
        dira_min=0.999,
        ltime_min=0.2 * picosecond,
        mipchi2_max=None,
        daughter_mipchi2_min=None,
        adoca_max=None,
        vtxchi2pdof_max=20.):

    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > asumpt_min, F.MAXDOCACHI2CUT(achi2doca_max))
    combination123_code = F.require_all(F.MAXDOCACHI2CUT(achi2doca_max))
    combination12_code = F.require_all(F.MAXDOCACHI2CUT(achi2doca_max))
    if MassWindow:
        combination_code &= (F.SUBCOMB(Functor=in_range(am12_min, F.MASS, am12_max), Indices=[1,2]) &
                             F.SUBCOMB(Functor=in_range(am12_min, F.MASS, am12_max), Indices=[3,4])) | \
                             (F.SUBCOMB(Functor=in_range(am12_min, F.MASS, am12_max), Indices=[1,4]) &
                              F.SUBCOMB(Functor=in_range(am12_min, F.MASS, am12_max), Indices=[2,3])) | \
                             (F.SUBCOMB(Functor=in_range(am123_min, F.MASS, am123_max), Indices=[1,2,3])) | \
                             (F.SUBCOMB(Functor=in_range(am123_min, F.MASS, am123_max), Indices=[1,2,4])) | \
                             (F.SUBCOMB(Functor=in_range(am123_min, F.MASS, am123_max), Indices=[1,3,4])) | \
                             (F.SUBCOMB(Functor=in_range(am123_min, F.MASS, am123_max), Indices=[2,3,4]))
    else:
        combination123_code &= in_range(am123_min, F.MASS, am123_max)
        combination12_code &= in_range(am12_min, F.MASS, am12_max)

    pvs = make_pvs()
    vertex_code = F.require_all(
        in_range(mothermass_min, F.MASS, mothermass_max), F.PT > motherpt_min,
        F.CHI2DOF < vtxchi2pdof_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
        F.BPVLTIME(pvs) > ltime_min,
        F.BPVDIRA(pvs) > dira_min)
    if mipchi2_max is not None:
        vertex_code &= F.MINIPCHI2(pvs) < mipchi2_max
    if daughter_mipchi2_min is not None:
        vertex_code &= F.MIN(F.MINIPCHI2(pvs)) > daughter_mipchi2_min
    if adoca_max is not None:
        combination12_code &= F.DOCA(1, 2) < adoca_max
        combination123_code &= F.require_all(
            F.DOCA(1, 3) < adoca_max,
            F.DOCA(2, 3) < adoca_max)
        combination_code &= F.require_all(
            F.DOCA(1, 4) < adoca_max,
            F.DOCA(2, 4) < adoca_max,
            F.DOCA(3, 4) < adoca_max)

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
        Combination123Cut=combination123_code,
        Combination12Cut=combination12_code)


@configurable
def make_lifetime_unbiased_b2x(particles,
                               descriptor,
                               name='BNOC_B2XLTUCombiner_{hash}',
                               am_min=5050 * MeV,
                               am_max=5650 * MeV,
                               am_min_vtx=5050 * MeV,
                               am_max_vtx=5650 * MeV,
                               sum_pt_min=5 * GeV,
                               vtx_chi2pdof_max=20.):  # was 10 in Run1+2
    '''
    LifeTime Unbiased B decay maker: defines default cuts and B mass range.
    '''
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > sum_pt_min)

    vertex_code = F.require_all(
        in_range(am_min_vtx, F.MASS, am_max_vtx), F.CHI2DOF < vtx_chi2pdof_max)

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


##############################################
# For BcTohhh and BuTohhh lines              #
##############################################


@configurable
def make_b2hhh(
        particles,
        descriptor,
        name='BNOC_B2HHHCombiner_{hash}',
        am_min=4950 * MeV,
        am_max=5750 * MeV,
        am_min_vtx=5000 * MeV,
        am_max_vtx=5700 * MeV,
        PT_sum_min=2.5 * GeV,
        FDCHI2=500,
        PVDOCAmin=3.0,
        IPCHI2_min=10,
        PVIPCHI2sum=500,
        vtx_chi2pdof_max=10,
        bpvipchi2_max=25,
        bpvltime_min=0.2 * picosecond,
        bpvdira_min=0.9999,
        adoca_max=0.2 * mm,
):
    '''
    Specialised 3-body Hb --> (h h h)  decay maker
    '''

    combination12_code = F.require_all(F.DOCA(1, 2) < adoca_max)

    pvs = make_pvs()
    combination_code = F.require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > PT_sum_min,
        F.SUM(F.MINIPCHI2(pvs)) > PVIPCHI2sum)

    vertex_code = F.require_all(
        in_range(am_min_vtx, F.MASS, am_max_vtx), F.CHI2DOF < vtx_chi2pdof_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
        F.BPVFDCHI2(pvs) > FDCHI2, F.MIN_ELEMENT @ F.ALLPV_FD(pvs) > PVDOCAmin,
        F.MINIPCHI2(pvs) < IPCHI2_min,
        F.BPVLTIME(pvs) > bpvltime_min,
        F.BPVDIRA(pvs) > bpvdira_min)

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=combination12_code,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


##############################################
# For bTohh lines                            #
##############################################


@configurable
def make_b2Lambdah(particles,
                   descriptor,
                   name='BNOC_b2LambdahCombiner_{hash}',
                   apt_min=1000.0 * MeV,
                   apt1_min=500.0 * MeV,
                   sumpt_min=800.0 * MeV,
                   sumpt_num_min=2,
                   am_min=4800. * MeV,
                   am_max=7000. * MeV,
                   acutdocachi2=5.0,
                   vt_pt=800.0 * MeV,
                   vt_vchi2pdof_max=12.,
                   vt_bpvdira_min=0.995,
                   mipchidv_max=15.,
                   bpvvdchi2=30.0):
    ''' 
    For B+->Lambda~0 p+, 
        Bc+->Lambda~0 p+
        Xi_b- -> Lambda0 pi-,
        Xi_b- -> Lambda0 K-
    '''

    combination_code = require_all(F.PT > apt_min,
                                   F.CHILD(1, F.PT) > apt1_min,
                                   F.SUM(F.PT > sumpt_min) >= sumpt_num_min,
                                   in_range(am_min, F.MASS, am_max),
                                   F.MAXDOCACHI2CUT(acutdocachi2))

    pvs = make_pvs()

    vertex_code = require_all(F.PT > vt_pt,
                              F.BPVDIRA(pvs) > vt_bpvdira_min,
                              F.CHI2DOF < vt_vchi2pdof_max,
                              F.MINIPCHI2(pvs) < mipchidv_max,
                              F.BPVFDCHI2(pvs) > bpvvdchi2)
    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


@configurable
def make_b2Kpi(particles,
               descriptor,
               name='BNOC_b2KpiCombiner_{hash}',
               comb_m_min=4000 * MeV,
               comb_m_max=6200 * MeV,
               comb_pt_min=5000 * MeV,
               mtdocachi2_max=8.0,
               pt_min=5000 * MeV):
    combination_code = require_all(
        in_range(comb_m_min, F.MASS, comb_m_max),
        F.SUM(F.PT) > comb_pt_min,
    )

    pvs = make_pvs()
    composite_code = require_all(
        F.MTDOCACHI2(1, pvs) < mtdocachi2_max,
        F.PT > pt_min,
    )
    return ParticleCombiner(
        particles,
        ParticleCombiner="ParticleAdder",
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=composite_code,
    )


##############################################
# For BuToKSh lines                          #
##############################################


@configurable
def make_b2ksh(particles,
               descriptor,
               name='BNOC_BC2KSHCombiner_{hash}',
               am_min=4800.0 * MeV,
               am_max=8000.0 * MeV,
               amed_pt_min=4000.0 * MeV,
               p_min=25000.0 * MeV,
               vchi2pdof_max=9.0,
               bpvdira_min=0.99,
               mipchi2_max=20.0,
               bpvfdchi2_min=20.0):

    med_pt_cut = ((F.CHILD(1, F.PT) + F.CHILD(2, F.PT)) > amed_pt_min)

    combination_code = F.require_all(
        in_range(am_min - 50 * MeV, F.MASS, am_max + 50 * MeV), med_pt_cut)
    pvs = make_pvs()

    vertex_code = F.require_all(F.P > p_min, in_range(am_min, F.MASS, am_max),
                                F.CHI2DOF < vchi2pdof_max,
                                F.BPVDIRA(pvs) > bpvdira_min,
                                F.MINIPCHI2(pvs) < mipchi2_max,
                                F.BPVFDCHI2(pvs) > bpvfdchi2_min)
    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


##############################################
# For BdTohh lines                           #
##############################################


@configurable
def make_b2hh(particles,
              descriptor,
              name='BNOC_B2HHCombiner_{hash}',
              am_min=4700 * MeV,
              am_max=6200 * MeV,
              sum_pt=4500 * MeV,
              docachi2=9.0,
              pt_min=1200 * MeV,
              dira_min=0.99,
              ipchi2_max=9,
              fdchi2_min=100):
    pvs = make_pvs()
    combination_cut = require_all(
        F.SUM(F.PT) > sum_pt,
        in_range(am_min, F.MASS, am_max),
        F.MAXDOCACHI2CUT(docachi2),
    )
    composite_cut = require_all(
        F.PT > pt_min,
        F.BPVDIRA(pvs) > dira_min,
        F.BPVIPCHI2(pvs) < ipchi2_max,
        F.BPVFDCHI2(pvs) > fdchi2_min,
    )
    return ParticleCombiner(
        particles,
        DecayDescriptor=descriptor,
        name=name,
        CombinationCut=combination_cut,
        CompositeCut=composite_cut)


##################################################
#### Builders for 2-/3-/4-body bbaryon decays ####
##################################################

### Currently most of the baryonic lines are Xibm/Ombm, so defualt mass window is tuned for those decays, so a different sent of values has to be passed manually when using for Lambda0/Xib0


@configurable
def make_bbaryon_2body(particles,
                       descriptor,
                       name,
                       mass_min=5395 * MeV,
                       mass_max=6305 * MeV,
                       pt_min=1500 * MeV,
                       pt_sum_min=3000 * MeV,
                       docachi2_max=25.,
                       adoca12_max=0.5 * mm,
                       vchi2pdof_max=5.,
                       ipchi2_max=20.,
                       bpvfdchi2_min=40.,
                       dira_min=0.9995,
                       bpvltime_min=0.1 * picosecond,
                       bcvtx_sep_min=None):
    '''Builder for two-body b-baryon decay.'''
    name = "BNOC_bbaryon_2body_{hash}"

    pvs = make_pvs()

    combination_cut = require_all(
        in_range(mass_min * 0.9, F.MASS, mass_max * 1.1),
        F.SUM(F.P) > pt_min,
        F.CHILD(1, F.PT) + F.CHILD(2, F.PT) > pt_sum_min,
        F.MAXDOCACHI2CUT(docachi2_max),
        F.DOCA(1, 2) < adoca12_max)

    composite_cut = require_all(
        in_range(mass_min, F.MASS, mass_max), F.CHI2DOF < vchi2pdof_max,
        F.BPVDIRA(pvs) > dira_min,
        F.BPVIPCHI2(pvs) < ipchi2_max,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        F.BPVLTIME(pvs) > bpvltime_min)

    # To be used only for lines with composite daughter (L0, Xi-, Omega-)
    if bcvtx_sep_min is not None:
        composite_cut &= F.CHILD(1, F.END_VZ) - F.END_VZ > bcvtx_sep_min

    return ParticleCombiner(
        Inputs=particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_cut,
        CompositeCut=composite_cut)


@configurable
def make_bbaryon_3body(particles,
                       descriptor,
                       name,
                       mass_min=5395 * MeV,
                       mass_max=6305 * MeV,
                       pt_min=1500 * MeV,
                       pt_sum_min=3000 * MeV,
                       docachi2_max=25.,
                       adoca12_max=0.5 * mm,
                       adoca13_max=0.5 * mm,
                       adoca23_max=0.5 * mm,
                       vchi2pdof_max=5.,
                       ipchi2_max=20.,
                       bpvfdchi2_min=40.,
                       dira_min=0.9995,
                       bpvltime_min=0.1 * picosecond,
                       bcvtx_sep_min=None):
    '''Builder for three-body b-baryon decay.'''
    name = "BNOC_bbaryon_3body_{hash}"

    pvs = make_pvs()

    combination12_code = require_all(
        F.DOCA(1, 2) < adoca12_max, F.MASS < mass_max)

    combination_cut = require_all(
        in_range(mass_min * 0.9, F.MASS, mass_max * 1.1),
        F.SUM(F.P) > pt_min,
        F.CHILD(1, F.PT) + F.CHILD(2, F.PT) + F.CHILD(3, F.PT) > pt_sum_min,
        F.MAXDOCACHI2CUT(docachi2_max),
        F.DOCA(1, 2) < adoca12_max,
        F.DOCA(1, 3) < adoca13_max,
        F.DOCA(2, 3) < adoca23_max)

    composite_cut = require_all(
        in_range(mass_min, F.MASS, mass_max), F.CHI2DOF < vchi2pdof_max,
        F.BPVDIRA(pvs) > dira_min,
        F.BPVIPCHI2(pvs) < ipchi2_max,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        F.BPVLTIME(pvs) > bpvltime_min)

    # To be used only for lines with composite daughter (L0, Xi-, Omega-)
    if bcvtx_sep_min is not None:
        composite_cut &= F.CHILD(1, F.END_VZ) - F.END_VZ > bcvtx_sep_min

    return ParticleCombiner(
        Inputs=particles,
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=combination12_code,
        CombinationCut=combination_cut,
        CompositeCut=composite_cut)


@configurable
def make_bbaryon_4body(particles,
                       descriptor,
                       name,
                       mass_min=5395 * MeV,
                       mass_max=6305 * MeV,
                       pt_min=1500 * MeV,
                       pt_sum_min=3000 * MeV,
                       docachi2_max=20.,
                       adoca12_max=0.5 * mm,
                       adoca13_max=0.5 * mm,
                       adoca14_max=0.5 * mm,
                       adoca23_max=0.5 * mm,
                       adoca24_max=0.5 * mm,
                       adoca34_max=0.5 * mm,
                       vchi2pdof_max=5.,
                       ipchi2_max=20.,
                       bpvfdchi2_min=40.,
                       dira_min=0.9995,
                       bpvltime_min=0.1 * picosecond,
                       bcvtx_sep_min=None):
    '''Builder for four-body b-baryon decay.'''
    name = "BNOC_bbaryon_4body_{hash}"

    pvs = make_pvs()

    combination12_code = require_all(
        F.DOCA(1, 2) < adoca12_max, F.MASS < mass_max)

    combination123_code = F.require_all(F.MASS < mass_max,
                                        F.DOCA(1, 3) < adoca13_max,
                                        F.DOCA(2, 3) < adoca23_max)

    combination_cut = require_all(
        in_range(mass_min * 0.9, F.MASS, mass_max * 1.1),
        F.SUM(F.P) > pt_min,
        F.CHILD(1, F.PT) + F.CHILD(2, F.PT) + F.CHILD(3, F.PT) + F.CHILD(
            4, F.PT) > pt_sum_min, F.MAXDOCACHI2CUT(docachi2_max),
        F.DOCA(1, 2) < adoca12_max,
        F.DOCA(1, 3) < adoca13_max,
        F.DOCA(1, 4) < adoca14_max,
        F.DOCA(2, 3) < adoca23_max,
        F.DOCA(2, 4) < adoca24_max,
        F.DOCA(3, 4) < adoca34_max)

    composite_cut = require_all(
        in_range(mass_min, F.MASS, mass_max), F.CHI2DOF < vchi2pdof_max,
        F.BPVDIRA(pvs) > dira_min,
        F.BPVIPCHI2(pvs) < ipchi2_max,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        F.BPVLTIME(pvs) > bpvltime_min)

    # To be used only for lines with composite daughter (L0, Xi-, Omega-)
    if bcvtx_sep_min is not None:
        composite_cut &= F.CHILD(1, F.END_VZ) - F.END_VZ > bcvtx_sep_min

    return ParticleCombiner(
        Inputs=particles,
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=combination12_code,
        Combination123Cut=combination123_code,
        CombinationCut=combination_cut,
        CompositeCut=composite_cut)


@configurable
def make_bbaryon_5body(particles,
                       descriptor,
                       name,
                       mass_min=5395 * MeV,
                       mass_max=6305 * MeV,
                       pt_min=1500 * MeV,
                       pt_sum_min=3000 * MeV,
                       docachi2_max=20.,
                       adoca12_max=0.5 * mm,
                       adoca13_max=0.5 * mm,
                       adoca14_max=0.5 * mm,
                       adoca23_max=0.5 * mm,
                       adoca24_max=0.5 * mm,
                       adoca34_max=0.5 * mm,
                       vchi2pdof_max=5.,
                       ipchi2_max=20.,
                       bpvfdchi2_min=40.,
                       dira_min=0.9995,
                       bpvltime_min=0.1 * picosecond,
                       bcvtx_sep_min=None):
    '''Builder for five-body b-baryon decay.'''
    name = "BNOC_bbaryon_5body_{hash}"

    pvs = make_pvs()

    combination12_code = require_all(
        F.DOCA(1, 2) < adoca12_max, F.MASS < mass_max)

    combination123_code = F.require_all(F.MASS < mass_max,
                                        F.DOCA(1, 3) < adoca13_max,
                                        F.DOCA(2, 3) < adoca23_max)

    combination_cut = require_all(
        in_range(mass_min * 0.9, F.MASS, mass_max * 1.1),
        F.SUM(F.P) > pt_min,
        F.CHILD(1, F.PT) + F.CHILD(2, F.PT) + F.CHILD(3, F.PT) + F.CHILD(
            4, F.PT) + F.CHILD(5, F.PT) > pt_sum_min,
        F.MAXDOCACHI2CUT(docachi2_max),
        F.DOCA(1, 2) < adoca12_max,
        F.DOCA(1, 3) < adoca13_max,
        F.DOCA(1, 4) < adoca14_max,
        F.DOCA(2, 3) < adoca23_max,
        F.DOCA(2, 4) < adoca24_max,
        F.DOCA(3, 4) < adoca34_max)

    composite_cut = require_all(
        in_range(mass_min, F.MASS, mass_max), F.CHI2DOF < vchi2pdof_max,
        F.BPVDIRA(pvs) > dira_min,
        F.BPVIPCHI2(pvs) < ipchi2_max,
        F.BPVFDCHI2(pvs) > bpvfdchi2_min,
        F.BPVLTIME(pvs) > bpvltime_min)

    # To be used only for lines with composite daughter (L0, Xi-, Omega-)
    if bcvtx_sep_min is not None:
        composite_cut &= F.CHILD(1, F.END_VZ) - F.END_VZ > bcvtx_sep_min

    return ParticleCombiner(
        Inputs=particles,
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=combination12_code,
        Combination123Cut=combination123_code,
        CombinationCut=combination_cut,
        CompositeCut=composite_cut)


@configurable
def make_b2kshh(particles,
                descriptor,
                name="BNOC_B2KSHHCombiner_{hash}",
                asumpt_min=4500.0 * MeV,
                amed_pt_min=1000.0 * MeV,
                hh_amaxpt_min=1000.0 * MeV,
                hh_amaxp_min=10.0 * GeV,
                adoca23_max=15.0,
                adoca12_max=25.0,
                adoca13_max=25.0,
                m_min=5000.0 * MeV,
                m_max=6000.0 * MeV,
                pt_min=1500 * MeV,
                vchi2pdof_max=12.0,
                ksvtx_sep_min=15.0 * mm,
                bpvipchi2_max=8.0,
                bpvvdchi2_min=50.0,
                bpvdira_min=0.999,
                hh_bpvipchi2_min=50.0,
                bpvipchi2_sum_min=300.0):
    """
    A generic 3body decay maker. Makes use of ThreeBodyCombiner
    to be more efficient, first making a DOCAcut on the *2 first particles in the
    decay descriptor*.

    Parameters
    ----------
    particles
        Maker algorithm instances for input particles.
    descriptor : string
        Decay descriptor to be reconstructed.
    make_pvs : callable
        Primary vertex maker function.
    Remaining parameters define thresholds for the selection.
    """
    combination12_cut = require_all(F.DOCACHI2(1, 2) < adoca12_max)

    combination_cut = require_all(
        in_range(m_min - 50 * MeV, F.MASS, m_max + 50 * MeV),
        F.SUM(F.PT) > asumpt_min,
        F.SUM(F.PT > amed_pt_min) >= 2,
        F.SUBCOMB(Functor=F.MAX(F.P), Indices=[2, 3]) > hh_amaxp_min,
        F.SUBCOMB(Functor=F.MAX(F.PT), Indices=[2, 3]) > hh_amaxpt_min,
        F.DOCACHI2(1, 3) < adoca13_max,
        F.DOCACHI2(2, 3) < adoca23_max)

    pvs = make_pvs()

    hh_ipchi2_cut = (
        (F.CHILD(2, F.BPVIPCHI2(pvs)) + F.CHILD(3, F.BPVIPCHI2(pvs))) >
        hh_bpvipchi2_min)
    sum_ipchi2_cut = (
        (F.SUM_RANGE @ F.MAP(F.BPVIPCHI2(pvs)) @ F.GET_ALL_BASICS) >
        bpvipchi2_sum_min)
    ksvtx_sep_cut = ((F.CHILD(1, F.END_VZ) - F.END_VZ) > ksvtx_sep_min)

    vertex_cut = require_all(F.PT > pt_min, in_range(m_min, F.MASS, m_max),
                             ksvtx_sep_cut, F.CHI2DOF < vchi2pdof_max,
                             F.BPVDIRA(pvs) > bpvdira_min,
                             F.BPVIPCHI2(pvs) < bpvipchi2_max,
                             F.BPVFDCHI2(pvs) > bpvvdchi2_min, hh_ipchi2_cut,
                             sum_ipchi2_cut)

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        Combination12Cut=combination12_cut,
        CombinationCut=combination_cut,
        CompositeCut=vertex_cut)


##############################################
# For BdsToPpPmKS lines                      #
##############################################


@configurable
def make_b2kspp(particles,
                descriptor,
                name='BNOC_b2kshhCombiner_{hash}',
                am_min=5000.0 * MeV,
                am_max=6000.0 * MeV,
                sum_pt_min=3.5 * GeV,
                pt_min=1500 * MeV,
                hh_amaxp_min=10.0 * GeV,
                amed_pt_min=1300.0 * MeV,
                vchi2pdof_max=12.0,
                bpvipchi2_max=9.0,
                bpvdira_min=0.999,
                bpvfdchi2_min=50.0):

    combination_code = F.require_all(
        in_range(0.99 * am_min, F.MASS, 1.01 * am_max),
        ((F.CHILD(1, F.PT) + F.CHILD(2, F.PT)) > amed_pt_min),
        F.SUBCOMB(Functor=F.MAX(F.P), Indices=[1, 2]) > hh_amaxp_min,
        F.SUM(F.PT) > sum_pt_min)
    pvs = make_pvs()

    vertex_code = F.require_all(F.PT > pt_min, in_range(
        am_min, F.MASS, am_max), F.CHI2DOF < vchi2pdof_max,
                                F.BPVIPCHI2(pvs) < bpvipchi2_max,
                                F.BPVDIRA(pvs) > bpvdira_min,
                                F.BPVFDCHI2(pvs) > bpvfdchi2_min)

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)
