###############################################################################
# (c) Copyright 2021-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Utilities used in BnoC selection lines

check_process: make sure the `process` argument is 'hlt2' or 'spruce'

"""

from PyConf.utilities import ConfigurationError

from inspect import getmembers, isfunction

from functools import partial

from Moore.config import register_line_builder
from Moore.lines import Hlt2Line, SpruceLine

from Hlt2Conf.lines.bnoc.prefilters import bnoc_prefilters

from Hlt2Conf.lines.bnoc.builders.basic_builder import Topo_prefilter_extra_outputs


def check_process(func):
    def wrapper(*args, process, **kwargs):
        if process not in ['hlt2', 'spruce']:
            raise ConfigurationError(
                '`process` should be either "hlt2" or "spruce".')
        return func(*args, process=process, **kwargs)

    return wrapper


#Taken directly from b_to_open_charm/utils.py
def update_makers(line_makers, line_module):
    '''Store line makers in a dictionary.

    Makers defined in independent modules (e.g. b_to_dh)
    is read by this function and updated as an item to a dictionary.
    The name of makers (e.g. make_BuToD0K_D0ToHH) is saved as the key,
    and the function itself is the value of item.

    This is achieved by `getmembers` from `inspect` package.

    Args:
        line_makers: Dictionary to save makers.
        line_module: From which module to update the makers.
    '''
    line_makers.update({
        k: v
        for k, v in dict(getmembers(line_module, isfunction)).items()
        if k.startswith('make')
    })


# Take inspiration from B2OC and provide functions to build the different line
# builders


# Default lines (no extra_outputs, TOPO, GEC prescale etc.)
def make_default_lines(process,
                       line_dict,
                       line_makers,
                       default_lines=None,
                       spruce_hlt2_filters=None):
    if not default_lines: return

    for decay in default_lines:
        make_generic_line(
            process=process,
            line_dict=line_dict,
            line_makers=line_makers,
            line=decay,
            spruce_hlt2_filters=spruce_hlt2_filters)


# Prescaled lines, as per default but with a prescale
def make_prescaled_lines(process,
                         line_dict,
                         line_makers,
                         prescaled_lines=None,
                         spruce_hlt2_filters=None):
    if not prescaled_lines: return

    for decay, arg_dict in sorted(prescaled_lines.items()):
        prescale = arg_dict['prescale']
        make_generic_line(
            process=process,
            line_dict=line_dict,
            line_makers=line_makers,
            line=decay,
            prescale=prescale,
            spruce_hlt2_filters=spruce_hlt2_filters)


# Flavour tagging lines
# need flavour tagging extra_outputs and persistreco=True
def make_flavour_tagging_lines(process,
                               line_dict,
                               line_makers,
                               flavour_tagging_lines=None,
                               spruce_hlt2_filters=None):
    if not flavour_tagging_lines: return
    for decay in flavour_tagging_lines:
        make_generic_line(
            process=process,
            line_dict=line_dict,
            line_makers=line_makers,
            line=decay,
            flavour_tagging=True,
            spruce_hlt2_filters=spruce_hlt2_filters)


# Custom lines, e.g. with GEC or Topo requirements, custom extra_outputs, etc.
def make_custom_lines(process,
                      line_dict,
                      line_makers,
                      custom_lines=None,
                      spruce_hlt2_filters=None):
    if not custom_lines: return
    for decay, kwargs in custom_lines.items():
        make_generic_line(
            process=process,
            line_dict=line_dict,
            line_makers=line_makers,
            line=decay,
            spruce_hlt2_filters=spruce_hlt2_filters,
            **kwargs)


# The function that can, in principle, make any type of line.
# This is called by all the above `make_XXX_lines` functions.
# 2(3) body mva cuts have default values but will not be applied unless
# require_topo=True
# Note: Does one line at a time rather than looping over a list!
#       This is essential due to the use of `partial`.
@check_process
def make_generic_line(process,
                      line_dict,
                      line_makers,
                      line=None,
                      prescale=1,
                      flavour_tagging=False,
                      persistreco=False,
                      require_GEC=False,
                      require_topo=False,
                      min_twobody_mva=0.1,
                      min_threebody_mva=0.1,
                      spruce_hlt2_filters=None):
    if not line: return

    if process == 'spruce':
        prefix = 'Spruce'
        if spruce_hlt2_filters is not None and line in spruce_hlt2_filters:
            Line = partial(
                SpruceLine, hlt2_filter_code=spruce_hlt2_filters[line])
        else:
            Line = SpruceLine
    elif process == 'hlt2':
        prefix = 'Hlt2'
        Line = Hlt2Line

    name = f'{prefix}BnoC_{line}'
    maker_name = f'make_{line}'

    # Start with the simpler cases
    if not flavour_tagging and not require_GEC and not require_topo:

        @register_line_builder(line_dict)
        def make_line(name=name,
                      maker_name=maker_name,
                      prescale=prescale,
                      persistreco=persistreco):
            line_alg = line_makers[maker_name](process=process)
            # line_alg may already be a list as it is more performant to also
            # return any intermediate composite particles from the make_{line}
            # function
            if type(line_alg) != list: line_alg = [line_alg]

            return Line(
                name=name,
                prescale=prescale,
                algs=bnoc_prefilters() + line_alg,
                persistreco=persistreco)

    # All other combinations will need some sort of extra_outputs
    else:

        @register_line_builder(line_dict)
        def make_line(name=name,
                      maker_name=maker_name,
                      prescale=prescale,
                      persistreco=persistreco):
            line_alg = line_makers[maker_name](process=process)
            # line_alg may already be a list as it is more performant to also
            # return any intermediate composite particles from the make_{line}
            # function
            if type(line_alg) != list: line_alg = [line_alg]

            extra_outputs = []
            if require_topo:
                extra_outputs += Topo_prefilter_extra_outputs(
                    min_twobody_mva=min_twobody_mva,
                    min_threebody_mva=min_threebody_mva)

            return Line(
                name=name,
                prescale=prescale,
                algs=bnoc_prefilters(
                    require_topo=require_topo,
                    require_GEC=require_GEC,
                    min_twobody_mva=min_twobody_mva,
                    min_threebody_mva=min_threebody_mva) + line_alg,
                persistreco=persistreco,
                tagging_particles=flavour_tagging,
                extra_outputs=extra_outputs)
