###############################################################################
# (c) Copyright 2019-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Definitions of the QEE sprucing lines.
"""

from PyConf import configurable

from Moore.config import register_line_builder, SpruceLine

from RecoConf.reconstruction_objects import make_pvs, upfront_reconstruction
from RecoConf.event_filters import require_pvs
from Hlt2Conf.standard_jets import make_onlytrack_particleflow
from GaudiKernel.SystemOfUnits import GeV
from Hlt2Conf.lines.qee.high_mass_dimuon import make_Z_cand, make_Z_cand_SingleNoMuID, make_Z_cand_DoubleNoMuID, make_Zss_cand
from Hlt2Conf.lines.qee.high_mass_dielec import make_Zee_cand, make_Zeess_cand
from Hlt2Conf.lines.qee.vjets import make_Z0jet_cand, make_ssdimuonjet_cand, make_Z0SVjet_cand, make_Wjet_cand, make_WSVjet_cand, make_Z0jetjet_cand, make_Z0SVjetSVjet_cand, make_Wjetjet_cand, make_WSVjetSVjet_cand
from Hlt2Conf.lines.qee.vjets import make_Z0_elec_jet_cand, make_Z0_elec_SVjet_cand, make_W_elec_jet_cand, make_W_elec_SVjet_cand, make_Z0_elec_jetjet_cand, make_Z0_elec_SVjetSVjet_cand, make_W_elec_jetjet_cand, make_W_elec_SVjetSVjet_cand
from Hlt2Conf.lines.qee.top_muon_elec import make_ttbar_MuE_cand, make_ttbar_MuEBjet_cand
from Hlt2Conf.lines.qee.diboson import make_WW_emu_cand, make_WZ_Zll_Wlnu_cand, make_ZZ_Zllplusll_cand, make_Wleptgamma_cand, make_Zllgamma_cand
from Hlt2Conf.lines.qee.wz_boson_rare_decays import make_WZRareDecay_HadronGamma_cand, make_WZRareDecay_DiMuonMeson_cand, make_WRareDecay_DiMuonPhiDsp_cand
from Hlt2Conf.lines.qee.qee_builders import muon_filter
from Hlt2Conf.lines.qee.single_high_pt_muon import make_isolated_muons
from Hlt2Conf.lines.qee.single_high_pt_electron import make_highpt_isolated_electrons, make_highpt_electrons_noextra, make_highpt_electrons
from Hlt2Conf.lines.qee.di_bjet import make_SVTagDijets_cand, make_dijets, make_Trijets_cand, make_TrijetsTwoSVTag_cand
from Hlt2Conf.lines.qee.qee_builders import make_Z_trkeff_cand

# Necessary imports to hlt2_filter robustly;
from inspect import signature
from Hlt2Conf.lines.qee.high_mass_dimuon import z_to_mu_mu_line, z_to_mu_mu_single_nomuid_line, z_to_mu_mu_double_nomuid_line, same_sign_dimuon_line
from Hlt2Conf.lines.qee.high_mass_dielec import z_to_e_e_line, same_sign_dielec_line
from Hlt2Conf.lines.qee.single_high_pt_muon import threshold_map as muon_thresholds
from Hlt2Conf.lines.qee.single_high_pt_muon import single_muon_highpt_line, single_muon_highpt_prescale_line, single_muon_highpt_iso_line, single_muon_highpt_nomuid_line
from Hlt2Conf.lines.qee.di_bjet import diLightjet10GeV_line, diLightjet15GeV_line, diLightjet20GeV_line, diLightjet25GeV_line, diLightjet30GeV_line, diLightjet35GeV_line
from Hlt2Conf.lines.qee.single_high_pt_electron import pt_thresholds as elec_pt_thresholds
from Hlt2Conf.lines.qee.single_high_pt_electron import ecal_deposit_fractions as elec_ecal_deposit_fractions
from Hlt2Conf.lines.qee.single_high_pt_electron import single_electron_highpt_line, single_electron_highpt_prescale_line, single_electron_highpt_iso_line, single_electron_veryhighpt_line
from Hlt2Conf.lines.qee.high_mass_dimuon_tracking_efficiency import all_lines as trkeff_lines

sprucing_lines = {}
sprucing_trkeff_lines = {}

trkeff_tag_linenames = [
    linename for linename in trkeff_lines.keys() if "Tag" in linename
]


def _hlt2_decision_name(line_defn):
    line_name = signature(line_defn).parameters['name'].default
    return f"{line_name}Decision"


################ Z -> ll lines ################
@register_line_builder(sprucing_lines)
@configurable
def z_to_mu_mu_sprucing_line(name='SpruceQEE_ZToMuMu', prescale=1):
    """Z0 boson decay to two muons line, both requiring ismuon, passthrough after Hlt2QEE_ZToMuMu"""

    line_alg = make_Z_cand()
    return SpruceLine(
        name=name,
        persistreco=True,
        algs=upfront_reconstruction() + [line_alg],
        hlt2_filter_code=_hlt2_decision_name(z_to_mu_mu_line),
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def z_to_mu_mu_single_nomuid_sprucing_line(
        name='SpruceQEE_ZToMuMu_SingleNoMuID', prescale=1):
    """Z0 boson decay to two muons line, where one requires ismuon, for MuonID efficiency studies. passthrough after Hlt2QEE_ZToMuMu_SingleNoMuID"""

    line_alg = make_Z_cand_SingleNoMuID()
    return SpruceLine(
        name=name,
        persistreco=True,
        algs=upfront_reconstruction() + [line_alg],
        hlt2_filter_code=_hlt2_decision_name(z_to_mu_mu_single_nomuid_line),
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def z_to_mu_mu_double_nomuid_sprucing_line(
        name='SpruceQEE_ZToMuMu_DoubleNoMuID', prescale=1):
    """Z0 boson decay to two muons line, where neither requires ismuon, passthrough after Hlt2QEE_ZToMuMu_DoubleNoMuID"""

    line_alg = make_Z_cand_DoubleNoMuID()
    return SpruceLine(
        name=name,
        persistreco=True,
        algs=upfront_reconstruction() + [line_alg],
        hlt2_filter_code=_hlt2_decision_name(z_to_mu_mu_double_nomuid_line),
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def same_sign_dimuon_sprucing_line(name='SpruceQEE_DiMuonSameSign',
                                   prescale=1):
    """Z0 boson decay to two same sign muons line, passthrough after Hlt2QEE_DiMuonSameSign"""

    line_alg = make_Zss_cand()
    return SpruceLine(
        name=name,
        persistreco=True,
        algs=upfront_reconstruction() + [line_alg],
        hlt2_filter_code=_hlt2_decision_name(same_sign_dimuon_line),
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def z_to_e_e_sprucing_line(name='SpruceQEE_ZToEE', prescale=1):
    """Z0 boson decay to two electrons line, passthrough after Hlt2QEE_ZToEE"""

    line_alg = make_Zee_cand()
    return SpruceLine(
        name=name,
        persistreco=True,
        algs=upfront_reconstruction() + [line_alg],
        hlt2_filter_code=_hlt2_decision_name(z_to_e_e_line),
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def same_sign_dielec_sprucing_line(name='SpruceQEE_DiElectronSameSign',
                                   prescale=1):
    """Z0 boson decay to two same sign electrons line, passthrough after Hlt2QEE_DiElectronSameSign"""

    line_alg = make_Zeess_cand()
    return SpruceLine(
        name=name,
        persistreco=True,
        algs=upfront_reconstruction() + [line_alg],
        hlt2_filter_code=_hlt2_decision_name(same_sign_dielec_line),
        prescale=prescale)


################ Z -> mumu trkeff lines ################
# TODO register these lines in sprucing_lines dictionary for running over 2024 HLT2-processed data.
# Not currently registered as the upstream HLT2 lines did not persist raw banks in 2023.
# see: https://gitlab.cern.ch/lhcb/Moore/-/issues/661
for trkeff_method in ["Downstream", "VeloMuon", "UTMuon"]:

    @register_line_builder(sprucing_trkeff_lines)
    @configurable
    def z_to_mu_mu_trkeff_tag_sprucing_lines(
            trkeff_method=trkeff_method,
            name=f'SpruceQEE_ZToMuMu_TrackEff_{trkeff_method}_Tag',
            prescale=1):
        """
        Z0 boson decay to two muons line, where one is a Tag and the other a Probe muon, for TrackEfficiency studies.
        passthrough after Hlt2QEE_TrackEff_ZToMuMu_{trkeff_method}_{probe=mup,mum}_Tag
        """
        line_alg = make_Z_trkeff_cand(trkeff_method=trkeff_method)
        hlt2_tag_lines_for_method = [
            string for string in trkeff_tag_linenames
            if trkeff_method in string
        ]
        hlt2_filters = [
            f"{line_name}Decision" for line_name in hlt2_tag_lines_for_method
        ]
        return SpruceLine(
            name=name,
            algs=upfront_reconstruction() + [line_alg],
            hlt2_filter_code=hlt2_filters,
            prescale=prescale)


################ Z + Jet lines ################
@register_line_builder(sprucing_lines)
@configurable
def z_jet_sprucing_line(name='SpruceQEE_ZJet', prescale=1):
    """Z0 boson decay to two muons + jet line"""

    line_alg = make_Z0jet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def z_jet_persistreco_sprucing_line(name='SpruceQEE_ZJetPersistReco',
                                    prescale=1):
    """Z0 boson decay to two muons + jet line full event information are persisted"""

    line_alg = make_Z0jet_cand()
    return SpruceLine(
        name=name,
        persistreco=True,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def ssdimuon_jet_persistreco_sprucing_line(
        name='SpruceQEE_SSDiMuonJetPersistReco', prescale=1):
    """same sign dimuons + jet line full event information are persisted"""

    line_alg = make_ssdimuonjet_cand()
    return SpruceLine(
        name=name,
        persistreco=True,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def z_svjet_sprucing_line(name='SpruceQEE_ZSVJet', prescale=1):
    """Z0 boson decay to two muons + SV jet line"""

    line_alg = make_Z0SVjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def z_jetjet_sprucing_line(name='SpruceQEE_ZJetJet', prescale=1):
    """Z0 boson decay to two muons + jet jet line"""

    line_alg = make_Z0jetjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def z_svjetsvjet_sprucing_line(name='SpruceQEE_ZSVJetSVJet', prescale=1):
    """Z0 boson decay to two muons + SV jet SV jet line"""

    line_alg = make_Z0SVjetSVjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale)


################ W + Jet lines ################
@register_line_builder(sprucing_lines)
@configurable
def w_jet_sprucing_line(name='SpruceQEE_WJet', prescale=1):
    """High pt muon + jet line"""

    line_alg = make_Wjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def w_svjet_sprucing_line(name='SpruceQEE_WSVJet', prescale=1):
    """High pt muon + SV jet line"""

    line_alg = make_WSVjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def w_jetjet_sprucing_line(name='SpruceQEE_WJetJet', prescale=1):
    """high pt muon + jet jet line"""

    line_alg = make_Wjetjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def w_svjetsvjet_sprucing_line(name='SpruceQEE_WSVJetSVJet', prescale=1):
    """high pt muon + SV jet SV jet line"""

    line_alg = make_WSVjetSVjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def zee_jet_sprucing_line(name='SpruceQEE_ZEEJet', prescale=1):
    """Z0 boson decay to two electrons + jet line"""

    line_alg = make_Z0_elec_jet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale)


################ Z -> ll + Jet lines ################
@register_line_builder(sprucing_lines)
@configurable
def zee_svjet_sprucing_line(name='SpruceQEE_ZEESVJet', prescale=1):
    """Z0 boson decay to two electrons + SV jet line"""

    line_alg = make_Z0_elec_SVjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def zee_jetjet_sprucing_line(name='SpruceQEE_ZEEJetJet', prescale=1):
    """Z0 boson decay to two electrons + jet jet line"""

    line_alg = make_Z0_elec_jetjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def zee_svjetsvjet_sprucing_line(name='SpruceQEE_ZEESVJetSVJet', prescale=1):
    """Z0 boson decay to two electrons + SV jet SV jet line"""

    line_alg = make_Z0_elec_SVjetSVjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale)


################ W -> l + Jet lines ################
@register_line_builder(sprucing_lines)
@configurable
def we_jet_sprucing_line(name='SpruceQEE_WEJet', prescale=1):
    """High pt electron + jet line"""

    line_alg = make_W_elec_jet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def we_svjet_sprucing_line(name='SpruceQEE_WESVJet', prescale=1):
    """High pt electron + SV jet line"""

    line_alg = make_W_elec_SVjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def we_jetjet_sprucing_line(name='SpruceQEE_WEJetJet', prescale=1):
    """high pt electrons + jet jet line"""

    line_alg = make_W_elec_jetjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def we_svjetsvjet_sprucing_line(name='SpruceQEE_WESVJetSVJet', prescale=1):
    """high pt electrons + SV jet SV jet line"""

    line_alg = make_W_elec_SVjetSVjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale)


################ ttbar Jet lines ################
@register_line_builder(sprucing_lines)
@configurable
def ttbar_to_mu_e_sprucing_line(name='SpruceQEE_TTbarToMuE', prescale=1):
    """ttbar to muon-electron line"""

    line_alg = make_ttbar_MuE_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def ttbar_to_mu_e_bjet_sprucing_line(name='SpruceQEE_TTbarToMuEBjet',
                                     prescale=1):
    """ttbar to muon-electron-bjet line"""

    line_alg = make_ttbar_MuEBjet_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale)


################ DiBoson lines ################
@register_line_builder(sprucing_lines)
@configurable
def WW_to_e_mu_sprucing_line(name='SpruceQEE_WWToMuE', prescale=1):
    """WW to e mu nu nu line"""

    line_alg = make_WW_emu_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        prescale=prescale)


for lepton_type1, lepton_type2, line_suffix in zip(
    ["mu", "mu", "e", "e"], ["e", "mu", "mu", "e"],
    ["MuMuE", "MuMuMu", "EEMu", "EEE"]):

    @register_line_builder(sprucing_lines)
    @configurable
    def DiBoson_WZ_lll_sprucing_line(name=f'SpruceQEE_WZTo{line_suffix}',
                                     lepton_type1=f'{lepton_type1}',
                                     lepton_type2=f'{lepton_type2}',
                                     prescale=1):
        f"""WZ to {lepton_type1} {lepton_type1} {lepton_type2} line"""

        line_alg = make_WZ_Zll_Wlnu_cand(
            lepton_type1=lepton_type1, lepton_type2=lepton_type2)
        return SpruceLine(
            name=name,
            algs=upfront_reconstruction() + [line_alg],
            prescale=prescale)


for lepton_type1, lepton_type2, line_suffix in zip(
    ["mu", "mu", "e", "e"], ["mu", "e", "e", "mu"],
    ["MuMuMuMu", "MuMuEE", "EEEE", "EEMuMu"]):

    @register_line_builder(sprucing_lines)
    @configurable
    def ZZ_to_llll_sprucing_line(name=f'SpruceQEE_ZZTo{line_suffix}',
                                 lepton_type1=f'{lepton_type1}',
                                 lepton_type2=f'{lepton_type2}',
                                 prescale=1):
        f"""ZZ to {lepton_type1} {lepton_type1} {lepton_type2} {lepton_type2} line"""

        line_alg = make_ZZ_Zllplusll_cand(
            lepton_type1=lepton_type1, lepton_type2=lepton_type2)
        return SpruceLine(
            name=name,
            algs=upfront_reconstruction() + [line_alg],
            prescale=prescale)


for lepton_type, lepton_suffix in zip(["mu", "e"], ["Mu", "E"]):
    for photon_type, line_suffix in zip(["LL", "DD", "All"], ["LL", "DD", ""]):

        @register_line_builder(sprucing_lines)
        @configurable
        def WGamma_to_lgamma_sprucing_line(
                name=f'SpruceQEE_WGammaTo{lepton_suffix}Photon{line_suffix}',
                lepton_type=f'{lepton_type}',
                photon_type=f'{photon_type}',
                prescale=1):
            f"""Wgamma to {lepton_type} gamma ({photon_type}) line"""

            line_alg = make_Wleptgamma_cand(
                lepton_type=lepton_type, photon_type=photon_type)
            pvs = make_pvs()
            return SpruceLine(
                name=name,
                algs=upfront_reconstruction() + [require_pvs(pvs), line_alg],
                prescale=prescale)

        @register_line_builder(sprucing_lines)
        @configurable
        def ZGamma_to_mu_mu_gamma_LL_sprucing_line(
                name=f'SpruceQEE_ZGammaTo{lepton_suffix}{lepton_suffix}Photon{line_suffix}',
                lepton_type=f'{lepton_type}',
                photon_type=f'{photon_type}',
                prescale=1):
            f"""Zgamma to {lepton_suffix} {lepton_suffix} gamma {line_suffix}"""

            line_alg = make_Zllgamma_cand(
                lepton_type=lepton_type, photon_type=photon_type)
            pvs = make_pvs()
            return SpruceLine(
                name=name,
                algs=upfront_reconstruction() + [require_pvs(pvs), line_alg],
                prescale=prescale)


################ W/Z RareDecay Hadron lines ################
@register_line_builder(sprucing_lines)
@configurable
def W_to_JPsi_Phi_Dsp_sprucing_line(name='SpruceQEE_DiMuonPhiDsp', prescale=1):
    """W -> JPsi phi(1020) D_s+"""

    pvs = make_pvs()
    line_alg = make_WRareDecay_DiMuonPhiDsp_cand(pvs)

    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(pvs), line_alg],
        prescale=prescale)


for decay_type in [
        "JetGamma", "DpGamma", "DspGamma", "LcpGamma", "XicpGamma", "BpGamma",
        "BcpGamma", "JPsiGamma", "UpsilonGamma", "DzGamma", "Xic0Gamma",
        "B0Gamma", "BsGamma", "LbGamma"
]:
    for photon_type, line_suffix in zip(["LL", "DD", "All"], ["LL", "DD", ""]):

        @register_line_builder(sprucing_lines)
        @configurable
        def WZRareDecay_HadronGamma_sprucing_line(
                name=f'SpruceQEE_{decay_type}_{line_suffix}',
                decay_type=f'{decay_type}',
                photon_type=f'{photon_type}',
                prescale=1):
            f"""{decay_type} ({photon_type} photons) spruce line"""

            pvs = make_pvs()
            line_alg = make_WZRareDecay_HadronGamma_cand(
                pvs, photon_type=photon_type, decay_type=decay_type)

            return SpruceLine(
                name=name,
                algs=upfront_reconstruction() + [require_pvs(pvs), line_alg],
                prescale=prescale)


for decay_type in ["JPsiDsp", "DiMuonDsp", "DiMuonPion"]:

    @register_line_builder(sprucing_lines)
    @configurable
    def WZRareDecay_HadronMuMu_sprucing_line(name=f'SpruceQEE_{decay_type}',
                                             decay_type=f'{decay_type}',
                                             prescale=1):
        f"""{decay_type} spruce line"""

        pvs = make_pvs()
        line_alg = make_WZRareDecay_DiMuonMeson_cand(
            pvs, decay_type=decay_type)

        return SpruceLine(
            name=name,
            algs=upfront_reconstruction() + [require_pvs(pvs), line_alg],
            prescale=prescale)


################ Single muon lines ################
@register_line_builder(sprucing_lines)
@configurable
def single_muon_highpt_sprucing_line(name='SpruceQEE_SingleHighPtMuon',
                                     prescale=1):
    """High PT Single Muon line, passthrough after Hlt2QEE_SingleHighPtMuon"""

    line_alg = muon_filter(min_pt=muon_thresholds["standard"])
    return SpruceLine(
        name=name,
        persistreco=True,
        algs=upfront_reconstruction() + [line_alg],
        hlt2_filter_code=_hlt2_decision_name(single_muon_highpt_line),
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def single_muon_highpt_prescale_sprucing_line(
        name='SpruceQEE_SingleHighPtMuonPrescale', prescale=1):
    """High PT single muon line with lower pT threshold, prescaled to reduce the rate, passthrough after Hlt2QEE_SingleHighPtMuonPrescale"""

    line_alg = muon_filter(min_pt=muon_thresholds["prescale"])
    return SpruceLine(
        name=name,
        persistreco=True,
        algs=upfront_reconstruction() + [line_alg],
        hlt2_filter_code=_hlt2_decision_name(single_muon_highpt_prescale_line),
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def single_muon_highpt_iso_sprucing_line(name='SpruceQEE_SingleHighPtMuonIso',
                                         prescale=1):
    """High PT single muon line with lower pT threshold, using isolation variables to reduce the rate, passthrough after Hlt2QEE_SingleHighPtMuonIso"""

    filtered_muons = muon_filter(min_pt=muon_thresholds["iso"])
    line_alg = make_isolated_muons(
        filtered_muons, pflow_output=make_onlytrack_particleflow)
    return SpruceLine(
        name=name,
        persistreco=True,
        algs=upfront_reconstruction() + [line_alg],
        hlt2_filter_code=_hlt2_decision_name(single_muon_highpt_iso_line),
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def single_muon_highpt_nomuid_sprucing_line(
        name='SpruceQEE_SingleHighPtMuonNoMuID', prescale=1):
    """High PT single muon line without ISMUON requirement, for background and MuonID efficiency studies, passthrough after Hlt2QEE_SingleHighPtMuonNoMuID"""

    line_alg = muon_filter(
        min_pt=muon_thresholds["nomuid"], require_muID=False)
    return SpruceLine(
        name=name,
        persistreco=True,
        algs=upfront_reconstruction() + [line_alg],
        hlt2_filter_code=_hlt2_decision_name(single_muon_highpt_nomuid_line),
        prescale=prescale)


################ Single electron lines ################
@register_line_builder(sprucing_lines)
@configurable
def single_electron_highpt_sprucing_line(name='SpruceQEE_SingleHighPtElectron',
                                         prescale=1,
                                         persistreco=True):
    """High PT Single Electron line, passthrough after Hlt2QEE_SingleHighPtElectron"""
    line_alg = make_highpt_electrons(
        min_electron_pt=elec_pt_thresholds["standard"],
        ecal_deposit_fraction=elec_ecal_deposit_fractions['standard'])
    return SpruceLine(
        name=name,
        persistreco=persistreco,
        algs=upfront_reconstruction() + [line_alg],
        hlt2_filter_code=_hlt2_decision_name(single_electron_highpt_line),
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def single_electron_highpt_prescale_sprucing_line(
        name='SpruceQEE_SingleHighPtElectronPrescale',
        prescale=1,
        persistreco=True):
    """High PT single electron line with lower pT threshold, prescaled to reduce the rate, passthrough after Hlt2QEE_SingleHighPtElectronPrescale"""
    line_alg = make_highpt_electrons(
        min_electron_pt=elec_pt_thresholds["prescale"],
        ecal_deposit_fraction=elec_ecal_deposit_fractions['standard'])
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [line_alg],
        persistreco=persistreco,
        hlt2_filter_code=_hlt2_decision_name(
            single_electron_highpt_prescale_line),
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def single_electron_highpt_iso_sprucing_line(
        name='SpruceQEE_SingleHighPtElectronIso', prescale=1,
        persistreco=True):
    """High PT single electron line with lower pT threshold, using isolation variables to reduce the rate, passthrough after Hlt2QEE_SingleHighPtElectronIso"""
    filtered_electrons = make_highpt_electrons(
        min_electron_pt=elec_pt_thresholds["iso"],
        ecal_deposit_fraction=elec_ecal_deposit_fractions['iso'])
    line_alg = make_highpt_isolated_electrons(
        filtered_electrons, pflow_output=make_onlytrack_particleflow)
    return SpruceLine(
        name=name,
        persistreco=persistreco,
        algs=upfront_reconstruction() + [line_alg],
        hlt2_filter_code=_hlt2_decision_name(single_electron_highpt_iso_line),
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def single_electron_vhighpt_sprucing_line(
        name='SpruceQEE_SingleVHighPtElectron', prescale=0.1,
        persistreco=True):
    """Very High PT single electron line with fewer ID requirements, passthrough after Hlt2QEE_SingleVHighPtElectron"""
    line_alg = make_highpt_electrons_noextra(
        min_electron_pt=elec_pt_thresholds["vhigh"])
    return SpruceLine(
        name=name,
        persistreco=persistreco,
        algs=upfront_reconstruction() + [line_alg],
        hlt2_filter_code=_hlt2_decision_name(single_electron_veryhighpt_line),
        prescale=prescale)


################ SV Di/Tri-Jet lines ################
@register_line_builder(sprucing_lines)
@configurable
def SVTagDijets_sprucing_line(name='SpruceQEE_SVTagDijets', prescale=1):
    """SV Tag Dijets sprucing line"""

    line_alg = make_SVTagDijets_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale)


for jet_pt_min, hlt2_dec in zip(["10", "15", "20", "25", "30", "35"], [
        diLightjet10GeV_line, diLightjet15GeV_line, diLightjet20GeV_line,
        diLightjet25GeV_line, diLightjet30GeV_line, diLightjet35GeV_line
]):

    @register_line_builder(sprucing_lines)
    @configurable
    def Dijets_sprucing_line(name=f'SpruceQEE_Dijets{jet_pt_min}{jet_pt_min}',
                             jet_pt_min=jet_pt_min,
                             hlt2_dec=hlt2_dec,
                             prescale=1):
        f"""Dijet sprucing line with jet_min_pt of {jet_pt_min} GeV per child"""

        line_alg = make_dijets(
            tagpair=(None, None),
            prod_pt_min=int(jet_pt_min) * GeV,
            min_dphi=1.5)
        return SpruceLine(
            name=name,
            algs=upfront_reconstruction() +
            [require_pvs(make_pvs()), line_alg],
            hlt2_filter_code=_hlt2_decision_name(hlt2_dec),
            prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def Trijets_sprucing_line(name='SpruceQEE_Trijets', prescale=1):
    """Trijets sprucing line"""

    line_alg = make_Trijets_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale)


@register_line_builder(sprucing_lines)
@configurable
def TrijetsTwoSVTag_sprucing_line(name='SpruceQEE_TrijetsTwoSVTag',
                                  prescale=1):
    """Trijets sprucing line"""

    line_alg = make_TrijetsTwoSVTag_cand()
    return SpruceLine(
        name=name,
        algs=upfront_reconstruction() + [require_pvs(make_pvs()), line_alg],
        prescale=prescale)
