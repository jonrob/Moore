###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Definition of some B2OC B2DDh lines
Lines added by Chen Chen, Ruiting Ma and Huanhuan Liu

Line algorithms constructed by a generic `make_b2ddh` builder
which parses the decay descriptors of each line.

All lines in this file have same cuts for child particles,
defined in `b2ddh_hlt2_kwargs` and `b2ddh_spruce_kwargs`.
Therefore builders in this file shouldn't be used for anyone else,
because tuning cut for these lines shouldn't affect others.

For other B2DDh lines, add them in `b_to_ddh.py`
"""

from GaudiKernel.SystemOfUnits import MeV, GeV, mm, picosecond

from Hlt2Conf.lines.b_to_open_charm.utils import check_process

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import d_builder

from RecoConf.reconstruction_objects import make_pvs

import Functors as F
from Functors.math import in_range
from Hlt2Conf.algorithms_thor import ParticleContainersMerger
from Hlt2Conf.algorithms_thor import ParticleCombiner
from Functors import require_all

####################################
# Tools to parse decay descriptors #
####################################


def std_notation(particle, builders):
    std_name = particle.replace("-", "+").replace("~", "")
    if std_name not in builders.keys():
        raise KeyError("{} not in particles: {}".format(
            std_name, builders.keys()))
    return std_name


def parse_descriptor(descriptors, KS0="LL"):
    """
    All descriptors must contain the same set and order of combined particles
    """
    descriptor = descriptors[0]
    descriptor = descriptor.replace("[", "").replace("]", "").replace(
        "cc", "").replace("CC", "")
    daus = descriptor.split("->")[1]
    daus_list = daus.split(" ")
    while "" in daus_list:
        daus_list.remove("")
    for i, dau in enumerate(daus_list):
        if dau == "KS0": daus_list[i] = "KS0_" + KS0
    return daus_list


b2ddh_builders = {
    "D+": d_builder.make_dplus_to_kpipi_or_kkpi,
    "D_s+": d_builder.make_dsplus_to_hhh,
    "D_s+_t": d_builder.make_dsplus_to_kpkmpip,
    "D0": d_builder.make_dzero_to_kpi_or_kpipipi,
    "D0kpi": d_builder.make_dzero_to_kpi,
    "D0k3pi": d_builder.make_dzero_to_kpipipi,
    "D0_hh": d_builder.make_dzero_to_hh,
    "D*(2010)+": d_builder.make_dstar_to_dzeropi_cf,
    "K*(892)0": basic_builder.make_kstar0,
    "KS0_DD": basic_builder.make_ks_DD,
    "KS0_LL": basic_builder.make_ks_LL,
    "K+": basic_builder.make_tight_kaons,
    "pi+": basic_builder.make_tight_pions,
    "phi(1020)": basic_builder.make_phi,
    "rho(770)0": basic_builder.make_rho0
}

b2ddh_hlt2_kwargs = {
    "rho(770)0": {
        "am_min": 250. * MeV,
        "am_max": 3000. * MeV,
        "pi_pidk_max": 5,
        "adoca12_max": 0.2 * mm,
    },
    "K*(892)0": {
        "am_min": 600. * MeV,
        "am_max": 2000. * MeV,
    },
    "phi(1020)": {
        "am_min": 900. * MeV,
        "am_max": 3000. * MeV,
    },
    #"D+": {
    #    "pi_pidk_max": 20,
    #    "k_pidk_min": -10,
    #},
    #"K+": {
    #    "k_pidk_min": 0,
    #},
}

b2ddh_spruce_kwargs = b2ddh_hlt2_kwargs


def _make_b2x(
        particles,
        descriptor,
        name="B2OCB2XCombiner_{hash}",
        am_min=5050 * MeV,
        am_max=5650 * MeV,
        am_min_vtx=5050 * MeV,
        am_max_vtx=5650 * MeV,
        sum_pt_min=5 * GeV,
        vtx_chi2pdof_max=20.,  # was 10 in Run1+2
        bpvipchi2_max=25.,
        bpvltime_min=0.2 * picosecond,
        bpvdira_min=0.999,
        bcvtx_sep_min=None,
        bcvtx_fromdst_sep_min=None,
        b2dstdsth_sep_min=None,
        b2dstdh_sep_min=None,
        b2ddh_sep_min=None):
    '''
    Default B decay maker: defines default cuts and B mass range.
    '''
    combination_code = require_all(
        in_range(am_min, F.MASS, am_max),
        F.SUM(F.PT) > sum_pt_min)
    pvs = make_pvs()
    vertex_code = require_all(
        in_range(am_min_vtx, F.MASS, am_max_vtx), F.CHI2DOF < vtx_chi2pdof_max,
        F.BPVIPCHI2(pvs) < bpvipchi2_max,
        F.BPVLTIME(pvs) > bpvltime_min,
        F.BPVDIRA(pvs) > bpvdira_min)

    if bcvtx_sep_min is not None:
        vertex_code &= (F.CHILD(1, F.END_VZ) - F.END_VZ) > bcvtx_sep_min

    if bcvtx_fromdst_sep_min is not None:
        vertex_code &= (F.CHILD(1, F.CHILD(1, F.END_VZ)) -
                        F.END_VZ) > bcvtx_fromdst_sep_min

    if b2dstdsth_sep_min is not None:
        vertex_code &= (
            F.CHILD(1, F.CHILD(1, F.END_VZ)) - F.END_VZ) > b2dstdsth_sep_min
        vertex_code &= (
            F.CHILD(2, F.CHILD(1, F.END_VZ)) - F.END_VZ) > b2dstdsth_sep_min

    if b2dstdh_sep_min is not None:
        vertex_code &= (
            F.CHILD(1, F.CHILD(1, F.END_VZ)) - F.END_VZ) > b2dstdh_sep_min
        vertex_code &= (F.CHILD(2, F.END_VZ) - F.END_VZ) > b2dstdh_sep_min

    return ParticleCombiner(
        particles,
        name=name,
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)


def make_b2x(particles,
             descriptors,
             name='B2OCB2XMerger_{hash}',
             **decay_arguments):
    assert len(descriptors) > 0
    b_hadrons = []
    for descriptor in descriptors:
        b_hadrons.append(
            _make_b2x(
                particles=particles, descriptor=descriptor, **decay_arguments))
    return ParticleContainersMerger(b_hadrons, name=name)


def make_b2ddh(
        process,
        descriptors,
        cuts=None,
        KS0="LL",
        D0ToHH=False,
        DsToKKPi=False,
        D0ToKpi=False,
        D0ToK3pi=False,
        b2dstdsth_sep_min=None,
        b2dstdh_sep_min=None,
        b2ddh_sep_min=None,
):
    assert KS0 in ['LL', 'DD']

    if cuts: kwargs_cuts = cuts
    elif process == 'spruce': kwargs_cuts = b2ddh_spruce_kwargs
    elif process == 'hlt2': kwargs_cuts = b2ddh_hlt2_kwargs

    daughters = parse_descriptor(descriptors, KS0=KS0)
    daughters = [std_notation(dau, b2ddh_builders) for dau in daughters]

    builders = {}
    for dau in daughters:
        if dau in builders.keys(): continue
        kwargs = kwargs_cuts.get(dau)
        if not kwargs: kwargs = {}
        if dau == 'D0' and D0ToHH:
            builders[dau] = b2ddh_builders['D0_hh'](**kwargs)
        elif (dau == 'D_s+' and DsToKKPi) or (dau == 'D_s-' and DsToKKPi):
            builders[dau] = b2ddh_builders['D_s+_t'](**kwargs)
        elif dau == 'D0' and D0ToKpi:
            builders[dau] = b2ddh_builders['D0kpi'](**kwargs)
        elif dau == 'D0' and D0ToK3pi:
            builders[dau] = b2ddh_builders['D0k3pi'](**kwargs)
        else:
            builders[dau] = b2ddh_builders[dau](**kwargs)

    particles = [builders[dau] for dau in daughters]

    return make_b2x(
        particles=particles,
        descriptors=descriptors,
        name='B2OCB2DDHBuilder_{hash}',
        sum_pt_min=6. * GeV,
        b2dstdsth_sep_min=b2dstdsth_sep_min,
        b2dstdh_sep_min=b2dstdh_sep_min,
        b2ddh_sep_min=b2ddh_sep_min)


#################################
# Definition of line algorithms #
#################################


@check_process
def make_BdToD0DK_D0ToKPiOrKPiPiPi_DToHHH(process):
    line_alg = make_b2ddh(
        process=process, descriptors=['B0 -> D0 D- K+', 'B0 -> D0 D+ K-'])
    return line_alg


@check_process
def make_BdToD0DPi_D0ToKPiOrKPiPiPi_DToHHH(process):
    line_alg = make_b2ddh(
        process=process, descriptors=['B0 -> D0 D- pi+', 'B0 -> D0 D+ pi-'])
    return line_alg


@check_process
def make_BdToDstD0K_DstToD0Pi_D0ToKPiOrKPiPiPi_D0ToKPiOrKPiPiPi(process):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D*(2010)+ D0 K-', 'B0 -> D*(2010)- D0 K+'])
    return line_alg


@check_process
def make_BdToDstD0Pi_DstToD0Pi_D0ToKPiOrKPiPiPi_D0ToKPiOrKPiPiPi(process):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D*(2010)+ D0 pi-', 'B0 -> D*(2010)- D0 pi+'])
    return line_alg


@check_process
def make_BdToDpDmKsDD_DpToHHH(process):
    line_alg = make_b2ddh(
        process=process, descriptors=['B0 -> D+ D- KS0'], KS0="DD")
    return line_alg


@check_process
def make_BdToDpDmKsLL_DpToHHH(process):
    line_alg = make_b2ddh(
        process=process, descriptors=['B0 -> D+ D- KS0'], KS0="LL")
    return line_alg


@check_process
def make_BdToDstDmKsDD_DstToD0Pi_D0ToKPiOrKPiPiPi_DmToHHH(process):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D*(2010)+ D- KS0', 'B0 -> D*(2010)- D+ KS0'],
        KS0="DD")
    return line_alg


@check_process
def make_BdToDstDmKSLL_DstToD0Pi_D0ToKPiOrKPiPiPi_DmToHHH(process):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D*(2010)+ D- KS0', 'B0 -> D*(2010)- D+ KS0'],
        KS0="LL")
    return line_alg


@check_process
def make_BdToDstpDstmKsDD_DstpToD0Pi_D0ToKPiorKPiPiPi(process):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D*(2010)+ D*(2010)- KS0'],
        KS0="DD")
    return line_alg


@check_process
def make_BdToDstpDstmKsLL_DstpToD0Pi_D0ToKPiorKPiPiPi(process):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D*(2010)+ D*(2010)- KS0'],
        KS0="LL")
    return line_alg


@check_process
def make_BdToD0D0KsDD_D0ToKPiOrKPiPiPi(process):
    line_alg = make_b2ddh(
        process=process, descriptors=['B0 -> D0 D0 KS0'], KS0="DD")
    return line_alg


@check_process
def make_BdToD0D0KsLL_D0ToKPiOrKPiPiPi(process):
    line_alg = make_b2ddh(
        process=process, descriptors=['B0 -> D0 D0 KS0'], KS0="LL")
    return line_alg


@check_process
def make_BdToDpDmKst_DpToHHH(process):
    line_alg = make_b2ddh(
        process=process,
        descriptors=["B0 -> D+ D- K*(892)0", "B0 -> D- D+ K*(892)~0"])
    return line_alg


@check_process
def make_BdToDstpDmKst_DstpToD0Pi_D0ToKPiOrKPiPiPi_DmToHHH(process):
    line_alg = make_b2ddh(
        process=process,
        descriptors=[
            "B0 -> D*(2010)+ D- K*(892)0", "B0 -> D*(2010)- D+ K*(892)~0"
        ])
    return line_alg


@check_process
def make_BdToDstmDpKst_DstmToD0Pi_D0ToKPiOrKPiPiPi_DpToHHH(process):
    line_alg = make_b2ddh(
        process=process,
        descriptors=[
            "B0 -> D*(2010)- D+ K*(892)0", "B0 -> D*(2010)+ D- K*(892)~0"
        ])
    return line_alg


@check_process
def make_BdToDspDsmKst_DspToHHH(process):
    cuts = {
        "D_s+": {
            "k_pidk_min": -1,
        },
    }
    line_alg = make_b2ddh(
        process=process,
        descriptors=["B0 -> D_s+ D_s- K*(892)0", "B0 -> D_s- D_s+ K*(892)~0"],
        cuts={
            **b2ddh_hlt2_kwargs,
            **cuts,
        },
        b2ddh_sep_min=(-0.5) * mm)
    return line_alg


@check_process
def make_BuToDpDmK_DpToHHH(process):
    line_alg = make_b2ddh(process=process, descriptors=["[B+ -> D+ D- K+]cc"])
    return line_alg


@check_process
def make_BuToDstDK_DstToD0Pi_D0ToKPiOrKPiPiPi_DToHHH(process):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['[B+ -> D*(2010)+ D- K+]cc', '[B+ -> D*(2010)- D+ K+]cc'])
    return line_alg


@check_process
def make_BuToDstpDstmK_DstToD0Pi_D0ToKPiOrKPiPiPi(process):
    cuts = {
        "D*(2010)+": {
            "k_pidk_min": -1,
        },
        "K+": {
            "k_pidk_min": -5,
        },
    }
    line_alg = make_b2ddh(
        process=process,
        descriptors=['[B+ -> D*(2010)+ D*(2010)- K+]cc'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **cuts,
        },
        b2dstdsth_sep_min=(-0.5) * mm)
    return line_alg


@check_process
def make_BuToDspDsmK_DspToHHH(process):
    cuts = {
        "D_s+": {
            "k_pidk_min": -1,
        },
        "K+": {
            "k_pidk_min": -5,
        },
    }
    line_alg = make_b2ddh(
        process=process,
        descriptors=["[B+ -> D_s+ D_s- K+]cc"],
        cuts={
            **b2ddh_hlt2_kwargs,
            **cuts,
        },
        b2ddh_sep_min=(-0.5) * mm)
    return line_alg


@check_process
def make_BuToD0D0K_D0ToKPiOrKPiPiPi(process):
    line_alg = make_b2ddh(
        process=process, descriptors=["B+ -> D0 D0 K+", "B- -> D0 D0 K-"])
    return line_alg


@check_process
def make_BuToDpDmPi_DpToHHH(process):
    line_alg = make_b2ddh(process=process, descriptors=["[B+ -> D+ D- pi+]cc"])
    return line_alg


@check_process
def make_BuToDstDPi_DstToD0Pi_D0ToKPiOrKPiPiPi_DToHHH(process):
    line_alg = make_b2ddh(
        process=process,
        descriptors=[
            '[B+ -> D*(2010)+ D- pi+]cc', '[B+ -> D*(2010)- D+ pi+]cc'
        ])
    return line_alg


@check_process
def make_BuToDstpDstmPi_DstpToD0Pi_D0ToKPiOrKPiPiPi(process):
    line_alg = make_b2ddh(
        process=process, descriptors=['[B+ -> D*(2010)+ D*(2010)- pi+]cc'])
    return line_alg


@check_process
def make_BuToDspDsmPi_DsToHHH(process):
    line_alg = make_b2ddh(
        process=process, descriptors=["[B+ -> D_s+ D_s- pi+]cc"])
    return line_alg


@check_process
def make_BuToD0D0Pi_D0ToKPiOrKPiPiPi(process):
    line_alg = make_b2ddh(
        process=process, descriptors=["B+ -> D0 D0 pi+", "B- -> D0 D0 pi-"])
    return line_alg


@check_process
def make_BuToD0DpKsDD_D0ToKPiOrKPiPiPi_DpToHHH(process):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B+ -> D0 D+ KS0', 'B- -> D0 D- KS0'],
        KS0="DD")
    return line_alg


@check_process
def make_BuToD0DpKsLL_D0ToKPiOrKPiPiPi_DpToHHH(process):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B+ -> D0 D+ KS0', 'B- -> D0 D- KS0'],
        KS0="LL")
    return line_alg


@check_process
def make_BuToDstpD0KsDD_DstpToD0Pi_D0ToKPiOrKPiPiPi_D0ToKPiOrKPiPiPi(process):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B+ -> D*(2010)+ D0 KS0', 'B- -> D*(2010)- D0 KS0'],
        KS0="DD")
    return line_alg


@check_process
def make_BuToDstpD0KsLL_DstpToD0Pi_D0ToKPiOrKPiPiPi_D0ToKPiOrKPiPiPi(process):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B+ -> D*(2010)+ D0 KS0', 'B- -> D*(2010)- D0 KS0'],
        KS0="LL")
    return line_alg


@check_process
def make_BuToD0DpKst_D0ToKPiOrKPiPiPi_DpToHHH(process):
    cuts = {
        "D0": {
            "k_pidk_min": -1,
        },
        "D+": {
            "k_pidk_min": -1,
        },
    }
    line_alg = make_b2ddh(
        process=process,
        descriptors=["B+ -> D0 D+ K*(892)0", "B- -> D0 D- K*(892)~0"],
        cuts={
            **b2ddh_hlt2_kwargs,
            **cuts,
        },
        b2ddh_sep_min=(-0.5) * mm)
    return line_alg


@check_process
def make_BuToDstpD0Kst_DstpToD0Pi_D0ToKPiOrKPiPiPi_D0ToKPiOrKPiPiPi(process):
    line_alg = make_b2ddh(
        process=process,
        descriptors=[
            "B+ -> D*(2010)+ D0 K*(892)0", "B- -> D*(2010)- D0 K*(892)~0"
        ])
    return line_alg


@check_process
def make_BuToDspDmPi_DspToHHH_DmToHHH(process):
    line_alg = make_b2ddh(
        process=process, descriptors=['[B+ -> D_s+ D- pi+]cc'])
    return line_alg


@check_process
def make_BuToDstmDspPi_DstmToD0Pi_D0ToKPiOrKPiPiPi_DspToHHH(process):
    cuts = {
        "D*(2010)+": {
            "k_pidk_min": -1,
        },
        "D_s+": {
            "k_pidk_min": -1,
        },
    }
    line_alg = make_b2ddh(
        process=process,
        descriptors=['[B+ -> D*(2010)- D_s+ pi+]cc'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **cuts,
        },
        b2dstdh_sep_min=(-0.5) * mm)
    return line_alg


@check_process
def make_BdToDsD0Pi_DsToHHH_D0ToKPiOrKPiPiPi(process):
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D_s+ D0 pi-', 'B0 -> D_s- D0 pi+'])
    return line_alg


@check_process
def make_BuToDsD0Phi_DsToHHH_D0ToKPiOrKPiPiPi(process):
    cuts = {
        "D_s+": {
            "k_pidk_min": -1,
        },
        "D0": {
            "k_pidk_min": -1,
        },
    }
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D_s+ D0 phi(1020)', 'B0 -> D_s- D0 phi(1020)'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **cuts,
        },
        b2ddh_sep_min=(-0.5) * mm)
    return line_alg


@check_process
def make_BdToD0D0Phi_D0ToKPiOrKPiPiPi(process):
    cuts = {
        "D0": {
            "k_pidk_min": -1,
        },
    }
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D0 D0 phi(1020)'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **cuts,
        },
        b2ddh_sep_min=(-0.5) * mm)
    return line_alg


@check_process
def make_BdToDpDmPhi_DpToHHH(process):
    cuts = {
        "D+": {
            "k_pidk_min": -1,
        },
    }
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D+ D- phi(1020)'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **cuts,
        },
        b2ddh_sep_min=(-0.5) * mm)
    return line_alg


@check_process
def make_BdToDsDPhi_DsToHHH_DToHHH(process):
    cuts = {
        "D_s+": {
            "k_pidk_min": -1,
        },
        "D+": {
            "k_pidk_min": -1,
        },
    }
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D_s+ D- phi(1020)', 'B0 -> D_s- D+ phi(1020)'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **cuts,
        },
        b2ddh_sep_min=(-0.5) * mm)
    return line_alg


@check_process
def make_BdToDspDsmPhi_DspToHHH(process):
    cuts = {
        "D_s+": {
            "k_pidk_min": -1,
        },
    }
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D_s+ D_s- phi(1020)'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **cuts,
        },
        b2ddh_sep_min=(-0.5) * mm)
    return line_alg


@check_process
def make_BdToDstDsPhi_DstToD0Pi_D0ToKPiOrKPiPiPi_DsToHHH(process):
    cuts = {
        "D*(2010)+": {
            "k_pidk_min": -1,
        },
        "D_s+": {
            "k_pidk_min": -1,
        }
    }
    line_alg = make_b2ddh(
        process=process,
        descriptors=[
            'B0 -> D*(2010)- D_s+ phi(1020)', 'B0 -> D*(2010)+ D_s- phi(1020)'
        ],
        cuts={
            **b2ddh_hlt2_kwargs,
            **cuts,
        },
        b2dstdh_sep_min=(-0.5) * mm)
    return line_alg


@check_process
def make_BdToDstpDstmPhi_DstpToD0Pi_D0ToKPiOrKPiPiPi(process):
    cuts = {
        "D*(2010)+": {
            "k_pidk_min": -1,
        },
    }

    line_alg = make_b2ddh(
        process=process,
        descriptors=['B0 -> D*(2010)+ D*(2010)- phi(1020)'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **cuts,
        },
        b2dstdsth_sep_min=(-0.5) * mm)
    return line_alg


@check_process
def make_BuToDstpDstmKst_DstpToD0Pi_D0ToKPiOrKPiPiPi(process):
    line_alg = make_b2ddh(
        process=process,
        descriptors=[
            'B+ -> D*(2010)+ D*(2010)- K*(892)0',
            'B- -> D*(2010)- D*(2010)+ K*(892)~0'
        ])
    return line_alg


@check_process
def make_BuToDsD0Rho0_DsToKKPi_D0ToKPi(process):
    cuts = {
        "D_s+": {
            "adoca12_max": 0.2,
            "adoca13_max": 0.2,
            "adoca23_max": 0.2,
        }
    }
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B+ -> D_s+ D0 rho(770)0', 'B- -> D_s- D0 rho(770)0'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **cuts
        },
        DsToKKPi=True,
        D0ToKpi=True)
    return line_alg


@check_process
def make_BuToDsD0Rho0_DsToKKPi_D0ToKPiPiPi(process):
    cuts = {
        "D_s+": {
            "adoca12_max": 0.2,
            "adoca13_max": 0.2,
            "adoca23_max": 0.2,
            "k_p_min": 3 * GeV,
            "k_pt_min": 500 * MeV,
        },
        "D0": {
            "k_p_min": 3 * GeV,
            "k_pt_min": 500 * MeV,
        }
    }
    line_alg = make_b2ddh(
        process=process,
        descriptors=['B+ -> D_s+ D0 rho(770)0', 'B- -> D_s- D0 rho(770)0'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **cuts
        },
        DsToKKPi=True,
        D0ToK3pi=True)
    return line_alg


@check_process
def make_BdToDspDmRho0_DspToKpKmPip_DmToHHH(process):
    cuts = {
        "D_s+": {
            "adoca12_max": 0.2,
            "adoca13_max": 0.2,
            "adoca23_max": 0.2,
        }
    }
    line_alg = make_b2ddh(
        process=process,
        descriptors=['[B0 -> D_s+ D- rho(770)0]cc'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **cuts
        },
        DsToKKPi=True)
    return line_alg


@check_process
def make_BdToDstmDspRho0_DstmToD0Pi_D0ToKPiOrKPiPiPi_DspToKpKmPip(process):
    cuts = {
        "D_s+": {
            "adoca12_max": 0.2,
            "adoca13_max": 0.2,
            "adoca23_max": 0.2,
        }
    }
    line_alg = make_b2ddh(
        process=process,
        descriptors=['[B0 -> D*(2010)- D_s+ rho(770)0]cc'],
        cuts={
            **b2ddh_hlt2_kwargs,
            **cuts
        },
        DsToKKPi=True)
    return line_alg


# excited dst to d gamma/pi0 lines,
# cannot make by generic builder.
# keep their definitions here
# since they come from same authors.
@check_process
def make_BdToDst0DspPi_Dst0ToD0Gamma_D0ToKPiOrKPiPiPi_DspToHHH(process):
    if process == 'spruce':
        dz = d_builder.make_dzero_to_kpi_or_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
        ds = d_builder.make_dsplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
        pion = basic_builder.make_tight_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        dz = d_builder.make_dzero_to_kpi_or_kpipipi()
        ds = d_builder.make_dsplus_to_hhh()
        pion = basic_builder.make_tight_pions()
    dst = d_builder.make_dzerost_to_dzerogamma(dz)
    line_alg = make_b2x(
        particles=[dst, ds, pion],
        descriptors=['B0 -> D*(2007)0 D_s+ pi-', 'B0 -> D*(2007)0 D_s- pi+'],
        sum_pt_min=6 * GeV)
    return line_alg


@check_process
def make_BdToDsstpD0Pi_DsstpToDspGamma_DspToHHH_D0ToKPiOrKPiPiPi(process):
    if process == 'spruce':
        dz = d_builder.make_dzero_to_kpi_or_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
        ds = d_builder.make_dsplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
        pion = basic_builder.make_tight_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        dz = d_builder.make_dzero_to_kpi_or_kpipipi()
        ds = d_builder.make_dsplus_to_hhh()
        pion = basic_builder.make_tight_pions()
    dsst = d_builder.make_dsst_to_dsplusgamma(ds)
    line_alg = make_b2x(
        particles=[dsst, dz, pion],
        descriptors=['B0 -> D*_s+ D0 pi-', 'B0 -> D*_s- D0 pi+'],
        sum_pt_min=6 * GeV)
    return line_alg


@check_process
def make_BuToDsstpDmPi_DsstpToDspGamma_DspToHHH_DmToHHH(process):
    if process == 'spruce':
        dp = d_builder.make_dplus_to_kpipi_or_kkpi(
            pi_pidk_max=None, k_pidk_min=None)
        ds = d_builder.make_dsplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
        pion = basic_builder.make_tight_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        dp = d_builder.make_dplus_to_kpipi_or_kkpi()
        ds = d_builder.make_dsplus_to_hhh()
        pion = basic_builder.make_tight_pions()
    dsst = d_builder.make_dsst_to_dsplusgamma(ds)
    line_alg = make_b2x(
        particles=[dsst, dp, pion],
        descriptors=['[B+ -> D*_s+ D- pi+]cc'],
        sum_pt_min=6 * GeV)
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Gamma_D0ToKPiOrKPiPiPi_D0ToKPiOrKPiPiPi(process):
    if process == 'spruce':
        dz = d_builder.make_dzero_to_kpi_or_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        dz = d_builder.make_dzero_to_kpi_or_kpipipi()
        kaon = basic_builder.make_tight_kaons()
    dst = d_builder.make_dzerost_to_dzerogamma(dz)
    line_alg = make_b2x(
        particles=[dst, dz, kaon],
        descriptors=['B+ -> D*(2007)0 D0 K+', 'B- -> D*(2007)0 D0 K-'],
        sum_pt_min=6 * GeV)
    return line_alg


@check_process
def make_BdToDst0DK_Dst0ToD0Gamma_D0ToKPiOrKPiPiPi_DToHHH(process):
    if process == 'spruce':
        dz = d_builder.make_dzero_to_kpi_or_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
        dp = d_builder.make_dplus_to_kpipi_or_kkpi(
            pi_pidk_max=None, k_pidk_min=None)
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        dz = d_builder.make_dzero_to_kpi_or_kpipipi()
        dp = d_builder.make_dplus_to_kpipi_or_kkpi()
        kaon = basic_builder.make_tight_kaons()
    dst = d_builder.make_dzerost_to_dzerogamma(dz)
    line_alg = make_b2x(
        particles=[dst, dp, kaon],
        descriptors=['B0 -> D*(2007)0 D+ K-', 'B0 -> D*(2007)0 D- K+'],
        sum_pt_min=6 * GeV)
    return line_alg


@check_process
def make_BuToDsD0Pi0Merged_DsToHHH_D0ToKPiOrKPiPiPi(process):
    if process == 'spruce':
        dz = d_builder.make_dzero_to_kpi_or_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
        ds = d_builder.make_dsplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
    else:
        dz = d_builder.make_dzero_to_kpi_or_kpipipi()
        ds = d_builder.make_dsplus_to_hhh()
    pi0 = basic_builder.make_merged_pi0s()
    line_alg = make_b2x(
        particles=[ds, dz, pi0],
        descriptors=['B+ -> D_s+ D0 pi0', 'B- -> D_s- D0 pi0'],
        sum_pt_min=6 * GeV)
    return line_alg


@check_process
def make_BuToDsD0Pi0Resolved_DsToHHH_D0ToKPiOrKPiPiPi(process):
    if process == 'spruce':
        dz = d_builder.make_dzero_to_kpi_or_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
        ds = d_builder.make_dsplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
    else:
        dz = d_builder.make_dzero_to_kpi_or_kpipipi()
        ds = d_builder.make_dsplus_to_hhh()
    pi0 = basic_builder.make_resolved_pi0s()
    line_alg = make_b2x(
        particles=[ds, dz, pi0],
        descriptors=['B+ -> D_s+ D0 pi0', 'B- -> D_s- D0 pi0'],
        sum_pt_min=6 * GeV)
    return line_alg


@check_process
def make_BuToDsD0Pi0Merged_DsToKKPi_D0ToKPiOrKPiPiPi(process):
    if process == 'spruce':
        dz = d_builder.make_dzero_to_kpi_or_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
        ds = d_builder.make_dsplus_to_kpkmpip(
            pi_pidk_max=None, k_pidk_min=None)
    else:
        dz = d_builder.make_dzero_to_kpi_or_kpipipi()
        ds = d_builder.make_dsplus_to_kpkmpip()
    pi0 = basic_builder.make_merged_pi0s()
    line_alg = make_b2x(
        particles=[ds, dz, pi0],
        descriptors=['B+ -> D_s+ D0 pi0', 'B- -> D_s- D0 pi0'],
        sum_pt_min=6 * GeV)
    return line_alg


@check_process
def make_BuToDsD0Pi0Resolved_DsToKKPi_D0ToKPiOrKPiPiPi(process):
    if process == 'spruce':
        dz = d_builder.make_dzero_to_kpi_or_kpipipi()
        ds = d_builder.make_dsplus_to_kpkmpip()
    else:
        dz = d_builder.make_dzero_to_kpi_or_kpipipi()
        ds = d_builder.make_dsplus_to_kpkmpip()
    pi0 = basic_builder.make_resolved_pi0s()
    line_alg = make_b2x(
        particles=[ds, dz, pi0],
        descriptors=['B+ -> D_s+ D0 pi0', 'B- -> D_s- D0 pi0'],
        sum_pt_min=6 * GeV)
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Merged_D0ToKPiOrKPiPiPi_D0ToKPiOrKPiPiPi(
        process):
    if process == 'spruce':
        dz = d_builder.make_dzero_to_kpi_or_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        dz = d_builder.make_dzero_to_kpi_or_kpipipi()
        kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz, pi0)
    line_alg = make_b2x(
        particles=[dst, dz, kaon],
        descriptors=['B+ -> D*(2007)0 D0 K+', 'B- -> D*(2007)0 D0 K-'],
        sum_pt_min=6 * GeV)
    return line_alg


@check_process
def make_BuToDst0D0K_Dst0ToD0Pi0Resolved_D0ToKPiOrKPiPiPi_D0ToKPiOrKPiPiPi(
        process):
    if process == 'spruce':
        dz = d_builder.make_dzero_to_kpi_or_kpipipi()
        kaon = basic_builder.make_tight_kaons()
    elif process == 'hlt2':
        dz = d_builder.make_dzero_to_kpi_or_kpipipi()
        kaon = basic_builder.make_tight_kaons()
    pi0 = basic_builder.make_resolved_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz, pi0)
    line_alg = make_b2x(
        particles=[dst, dz, kaon],
        descriptors=['B+ -> D*(2007)0 D0 K+', 'B- -> D*(2007)0 D0 K-'],
        sum_pt_min=6 * GeV)
    return line_alg


@check_process
def make_BdToDst0DK_Dst0ToD0Pi0Merged_D0ToKPiOrKPiPiPi_DToHHH(process):
    dz = d_builder.make_dzero_to_kpi_or_kpipipi()
    dp = d_builder.make_dplus_to_kpipi_or_kkpi()
    kaon = basic_builder.make_tightpid_tight_kaons()
    pi0 = basic_builder.make_merged_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz, pi0)
    line_alg = make_b2x(
        particles=[dst, dp, kaon],
        descriptors=['B0 -> D*(2007)0 D+ K-', 'B0 -> D*(2007)0 D- K+'],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(-0.5) * mm)
    return line_alg


@check_process
def make_BdToDst0DK_Dst0ToD0Pi0Resolved_D0ToKPiOrKPiPiPi_DToHHH(process):
    dz = d_builder.make_dzero_to_kpi_or_kpipipi()
    dp = d_builder.make_dplus_to_kpipi_or_kkpi()
    kaon = basic_builder.make_tightpid_tight_kaons()
    pi0 = basic_builder.make_resolved_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz, pi0)
    line_alg = make_b2x(
        particles=[dst, dp, kaon],
        descriptors=['B0 -> D*(2007)0 D+ K-', 'B0 -> D*(2007)0 D- K+'],
        sum_pt_min=6 * GeV,
        b2dstdh_sep_min=(-0.5) * mm)
    return line_alg


@check_process
def make_BdToDst0DsPi_Dst0ToD0Pi0Merged_D0ToKPiOrKPiPiPi_DsToHHH(process):
    if process == 'spruce':
        dz = d_builder.make_dzero_to_kpi_or_kpipipi(
            pi_pidk_max=None, k_pidk_min=None)
        ds = d_builder.make_dsplus_to_hhh(pi_pidk_max=None, k_pidk_min=None)
        pion = basic_builder.make_tight_pions(pi_pidk_max=None)
    elif process == 'hlt2':
        dz = d_builder.make_dzero_to_kpi_or_kpipipi()
        ds = d_builder.make_dsplus_to_hhh()
        pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_merged_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz, pi0)
    line_alg = make_b2x(
        particles=[dst, ds, pion],
        descriptors=['B0 -> D*(2007)0 D_s+ pi-', 'B0 -> D*(2007)0 D_s- pi+'],
        sum_pt_min=6 * GeV)
    return line_alg


@check_process
def make_BdToDst0DsPi_Dst0ToD0Pi0Resolved_D0ToKPiOrKPiPiPi_DsToHHH(process):
    if process == 'spruce':
        dz = d_builder.make_dzero_to_kpi_or_kpipipi()
        ds = d_builder.make_dsplus_to_hhh()
        pion = basic_builder.make_tight_pions()
    elif process == 'hlt2':
        dz = d_builder.make_dzero_to_kpi_or_kpipipi()
        ds = d_builder.make_dsplus_to_hhh()
        pion = basic_builder.make_tight_pions()
    pi0 = basic_builder.make_resolved_pi0s()
    dst = d_builder.make_dzerost_to_dzeropi0(dz, pi0)
    line_alg = make_b2x(
        particles=[dst, ds, pion],
        descriptors=['B0 -> D*(2007)0 D_s+ pi-', 'B0 -> D*(2007)0 D_s- pi+'],
        sum_pt_min=6 * GeV)
    return line_alg
