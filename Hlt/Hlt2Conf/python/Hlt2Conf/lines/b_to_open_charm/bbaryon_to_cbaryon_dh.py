###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
* Definition of B2OC beauty baryon lines
"""
from GaudiKernel.SystemOfUnits import GeV, MeV

import Functors as F

from Hlt2Conf.lines.b_to_open_charm.utils import check_process

from Hlt2Conf.lines.b_to_open_charm.builders import basic_builder
from Hlt2Conf.lines.b_to_open_charm.builders import cbaryon_builder
from Hlt2Conf.lines.b_to_open_charm.builders import b_builder
from Hlt2Conf.lines.b_to_open_charm.builders import d_builder

###########################################################
# Form the Xi_bc0 -> Lc+ D0 pi-,  Lc+ -> p K pi,  D0 --> K- pi+
# NB: Here, D0 == K- pi+ AND K+ pi-
##########################################################


@check_process
def make_Xibc0ToLcpD0Pi_LcpToPKPi_D0ToKPiOrKPiPiPi(process):
    if process == 'spruce':
        cmeson = d_builder.make_dzero_to_kpi_or_kpipipi()
        cbaryon = cbaryon_builder.make_lc_to_pkpi()
        pions = basic_builder.make_pions()
    elif process == 'hlt2':
        cmeson = d_builder.make_tight_dzero_to_kpi_or_kpipipi_for_xibc()
        cbaryon = cbaryon_builder.make_tight_lc_to_pkpi_for_xibc()
        pions = basic_builder.make_tightpid_tight_pions()

    ### applying hard cuts on pion if it's not forming Sigmac with Lc
    M13 = F.SUBCOMB(Functor=F.MASS, Indices=[1, 3])
    Sc_13 = ((M13 - F.CHILD(1, F.MASS)) < 300 * MeV)
    tight3 = ((F.CHILD(3, F.PT) > 500 * MeV) & (F.CHILD(3, F.P) > 5 * GeV))

    comb_cut_add = (Sc_13 | tight3)

    line_alg = b_builder.make_xibc2ccx(
        particles=[cbaryon, cmeson, pions],
        descriptors=[
            'Xi_bc0 -> Lambda_c+ D0 pi-', 'Xi_bc~0 -> Lambda_c~- D0 pi+'
        ],
        comb_cut_add=comb_cut_add,
    )
    return line_alg


######################################################################
# Form the Omega_bc0 -> Xi_c0 D0,  Xi_c0 -> p K K pi,  D0 --> K- pi+
# NB: Here, D0 == K- pi+ AND K+ pi- (provides a WS sample)
######################################################################


@check_process
def make_Ombc0ToLcpD0K_LcpToPKPi_D0ToKPiOrKPiPiPi(process):
    if process == 'spruce':
        cmeson = d_builder.make_dzero_to_kpi_or_kpipipi()
        cbaryon = cbaryon_builder.make_lc_to_pkpi()
        kaons = basic_builder.make_kaons()
    elif process == 'hlt2':
        cmeson = d_builder.make_tight_dzero_to_kpi_or_kpipipi_for_xibc()
        cbaryon = cbaryon_builder.make_tight_lc_to_pkpi_for_xibc()
        kaons = basic_builder.make_tightpid_tight_kaons(k_pidk_min=-2)
    line_alg = b_builder.make_xibc2ccx(
        particles=[cbaryon, cmeson, kaons],
        descriptors=[
            'Omega_bc0 -> Lambda_c+ D0 K-', 'Omega_bc~0 -> Lambda_c~- D0 K+'
        ])
    return line_alg


######################################################################
# Form the Xib0 -> Xic+ D0 K-, D0 -> Kpi or K3pi
######################################################################


@check_process
def make_Xib0ToXicpD0K_XicpToPKPi_D0ToKPi(process):
    if process == 'spruce':
        dz = d_builder.make_dzero_to_kpi(pi_pidk_max=None, k_pidk_min=None)
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        dz = d_builder.make_dzero_to_kpi()
        kaon = basic_builder.make_tight_kaons()
    xic = cbaryon_builder.make_xicp_to_pkpi()
    line_alg = b_builder.make_xib(
        particles=[xic, dz, kaon],
        descriptors=['Xi_b0 -> Xi_c+ D0 K-', 'Xi_b~0 -> Xi_c~- D0 K+'])
    return line_alg


@check_process
def make_Xib0ToXicpD0K_XicpToPKPi_D0ToKPiPiPi(process):
    if process == 'spruce':
        dz = d_builder.make_dzero_to_kpipipi(pi_pidk_max=None, k_pidk_min=None)
        kaon = basic_builder.make_tight_kaons(k_pidk_min=None)
    elif process == 'hlt2':
        dz = d_builder.make_dzero_to_kpipipi()
        kaon = basic_builder.make_tight_kaons()
    xic = cbaryon_builder.make_xicp_to_pkpi()
    line_alg = b_builder.make_xib(
        particles=[xic, dz, kaon],
        descriptors=['Xi_b0 -> Xi_c+ D0 K-', 'Xi_b~0 -> Xi_c~- D0 K+'])
    return line_alg
