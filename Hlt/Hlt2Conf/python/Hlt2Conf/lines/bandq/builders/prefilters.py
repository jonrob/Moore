###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Prefilters used by all lines: require at least one PV, GEC not used here. Upfront reco will be removed.
"""

from RecoConf.event_filters import require_pvs
from RecoConf.reconstruction_objects import (make_pvs, upfront_reconstruction)


def make_prefilters():
    return upfront_reconstruction() + [require_pvs(make_pvs())]
