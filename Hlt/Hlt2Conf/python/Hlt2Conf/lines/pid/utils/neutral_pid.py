###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Neutral PID Hlt2 particle filters/combiners for the following:
exclusive B -> hh gamma builder for KstG and PhiG
photons with high pt cut

Run2 TurCal ref: https://cds.cern.ch/record/2764341/files/LHCb-INT-2021-002.pdf?
author: Biplab Dey
date: 9.05.22
"""
from GaudiKernel.SystemOfUnits import GeV, MeV
import Functors as F
from Functors.math import in_range

from PyConf import configurable

from Hlt2Conf.standard_particles import (make_photons)

from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter

# for Bd->KstG and Bs->PhiG


@configurable
def make_exclusive_highpt_photons(
        name='calib_high_pt_photons_{hash}',
        make_particles=make_photons,
        et_min=2.0 * GeV
):  # loose, aligned with RD Hlt2RD_BdToKstGamma_Line. Run2 TurCal had 2.6 GeV.
    """Exclusive HighPt Photons"""

    code = F.require_all(F.PT > et_min)
    return ParticleFilter(make_particles(), F.FILTER(code), name=name)


@configurable
def make_b2xgamma_excl(intermediate, photons, pvs, descriptor, comb_m_min,
                       comb_m_max, pt_min, dira_min, bpv_ipchi2_max, name):
    combination_code = F.require_all(
        F.PT > pt_min,
        in_range(comb_m_min - 100. * MeV, F.MASS, comb_m_max + 100. * MeV),
    )
    vertex_code = F.require_all(
        in_range(comb_m_min, F.MASS, comb_m_max), F.PT > pt_min,
        F.BPVDIRA(pvs) > dira_min,
        F.BPVIPCHI2(pvs) < bpv_ipchi2_max)
    # return particle combiner
    return ParticleCombiner(
        name=name,
        Inputs=[intermediate, photons],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code)
