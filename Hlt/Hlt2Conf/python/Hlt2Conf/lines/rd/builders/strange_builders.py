###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Builders for strange decays. In this file we define constructors for particles
originating from strange decays. We provide two sets of requirements for basic
particles, normal and tight, which are chosen for rare and more abundant
decay signatures, respectively. For combinations, we distinguish between
those in which the parent particle comes from a primary vertex, and those
in which the combination of the decay products do not point to any primary
vertex (tipically semileptonic decays). The mass bounds are centralized in
dedicated configurable functions in order to consistently modify lines where
the mass bounds are complementary.

Contact: Miguel Ramos Pernas (miguel.ramos.pernas@cern.ch)
"""
import functools
import Functors as F
from Functors.math import in_range
from GaudiKernel.SystemOfUnits import GeV, MeV, mm, ns

from Hlt2Conf.standard_particles import (
    make_ismuon_long_muon,
    make_long_muons,
    make_long_electrons_no_brem,
    make_long_electrons_with_brem,
    make_long_pions,
    make_long_kaons,
    make_long_protons,
)
from Hlt2Conf.algorithms_thor import ParticleCombiner, ParticleFilter
from PyConf.Algorithms import (
    LHCb__Phys__ParticleMakers__PhotonMaker as PhotonMaker,
    LHCb__Phys__ParticleMakers__MergedPi0Maker as MergedPi0Maker)
from RecoConf.reconstruction_objects import make_neutral_protoparticles

from PyConf.Algorithms import FunctionalDiElectronMaker
from PyConf import configurable

# Monitoring variables in different scenarios
MONITORING_VARIABLES_CONTROL_NO_VERTEX = ('m', 'pt', 'eta')
MONITORING_VARIABLES_CONTROL = ('m', 'pt', 'eta', 'vchi2', 'ipchi2')
MONITORING_VARIABLES_SEARCH = ('pt', 'eta', 'vchi2', 'ipchi2')


def optional_functor(function):
    """ Wrap a function so it returns None if the first argument is None """

    def wrapper(value, *args):
        return None if value is None else function(value, *args)

    return wrapper


# empty requirements generator
NO_REQUIREMENTS = lambda: {}

# functors used for filtering
MIN_P = optional_functor(lambda v: F.P > v)
MIN_PT = optional_functor(lambda v: F.PT > v)
MAX_CHI2DOF = optional_functor(lambda v: F.CHI2DOF < v)
MAX_GHOSTPROB = optional_functor(lambda v: F.GHOSTPROB < v)
MAX_MINIP = optional_functor(lambda v, pvs: F.MINIP(pvs) < v)
MIN_MINIP = optional_functor(lambda v, pvs: F.MINIP(pvs) > v)
MAX_MINIPCHI2 = optional_functor(lambda v, pvs: F.MINIPCHI2(pvs) < v)
MIN_MINIPCHI2 = optional_functor(lambda v, pvs: F.MINIPCHI2(pvs) > v)
MIN_PID_MU = optional_functor(lambda v: F.PID_MU > v)
MIN_PID_P = optional_functor(lambda v: F.PID_P > v)
MIN_PID_P_OVER_K = optional_functor(lambda v: (F.PID_P - F.PID_K) > v)
MIN_PID_E = optional_functor(lambda v: F.PID_E > v)
MAX_DOCA = optional_functor(lambda v: F.MAXDOCACUT(v))
MAX_DOCACHI2 = optional_functor(lambda v: F.MAXDOCACHI2CUT(v))
MIN_BPVVDZ = optional_functor(lambda v, pvs: F.BPVVDZ(pvs) > v)
MAX_IPBPVVDZ_RATIO = optional_functor(
    lambda v, pvs: F.MINIP(pvs) / F.BPVVDZ(pvs) < v)
MIN_BPVVDRHO = optional_functor(lambda v, pvs: F.BPVVDRHO(pvs) > v)
MIN_RHO = optional_functor(lambda v: F.END_VX**2 + F.END_VY**2 > v**2)
MIN_BPVLTIME = optional_functor(lambda v, pvs: F.BPVLTIME(pvs) > v)
MIN_DIRA = optional_functor(lambda v, pvs: F.BPVDIRA(pvs) > v)
MIN_IS_NOT_H = optional_functor(lambda v: F.IS_NOT_H > v)
MIN_IS_PHOTON = optional_functor(lambda v: F.IS_PHOTON > v)


def used_functors(*args):
    """ Filter optional functors """
    return list(filter(lambda f: f is not None, args))


@configurable
def filter_decay_products(particles,
                          pvs,
                          *,
                          p_min=None,
                          pt_min=None,
                          track_chi2dof_max=None,
                          ghostprob_max=None,
                          ip_max=None,
                          ip_min=None,
                          ipchi2_max=None,
                          ipchi2_min=None,
                          pid_mu_min=None,
                          pid_p_min=None,
                          pid_p_over_k_min=None,
                          pid_e_min=None,
                          is_not_h_min=None,
                          is_photon_min=None,
                          name):
    """ Filter the products of strange decays """
    selection = used_functors(
        MIN_P(p_min),
        MIN_PT(pt_min),
        MAX_CHI2DOF(track_chi2dof_max),
        MAX_GHOSTPROB(ghostprob_max),
        MAX_MINIP(ip_max, pvs),
        MIN_MINIP(ip_min, pvs),
        MAX_MINIPCHI2(ipchi2_max, pvs),
        MIN_MINIPCHI2(ipchi2_min, pvs),
        MIN_PID_MU(pid_mu_min),
        MIN_PID_P(pid_p_min),
        MIN_PID_P_OVER_K(pid_p_over_k_min),
        MIN_PID_E(pid_e_min),
        MIN_IS_NOT_H(is_not_h_min),
        MIN_IS_PHOTON(is_photon_min),
    )
    return ParticleFilter(
        particles,
        F.FILTER(F.ALL if not selection else F.require_all(*selection)),
        name=name)


@configurable
def combination_requirements(
        pvs,
        *,
        # combination
        mass_range,
        doca_max=None,
        docachi2_max=None,
        # vertex selection
        vchi2pdof_max=None,
        bpvvdz_min=None,
        ip_bpvvdz_ratio_max=None,
        bpvvdrho_min=None,
        rho_min=None,
        ip_max=None,
        ip_min=None,
        ipchi2_max=None,
        ipchi2_min=None,
        dira_min=None,
        lifetime_min=None):
    """ Build the combination and vertex requirements for a trigger line """
    comb_mass_min, comb_mass_max = mass_range

    combination_selection = [in_range(comb_mass_min, F.MASS, comb_mass_max)
                             ] + used_functors(
                                 MAX_DOCA(doca_max),
                                 MAX_DOCACHI2(docachi2_max),
                             )

    vertex_selection = used_functors(
        MAX_CHI2DOF(vchi2pdof_max),
        MIN_BPVVDZ(bpvvdz_min, pvs),
        MAX_IPBPVVDZ_RATIO(ip_bpvvdz_ratio_max, pvs),
        MIN_BPVVDRHO(bpvvdrho_min, pvs),
        MIN_RHO(rho_min),
        MAX_MINIP(ip_max, pvs),
        MIN_MINIP(ip_min, pvs),
        MAX_MINIPCHI2(ipchi2_max, pvs),
        MIN_MINIPCHI2(ipchi2_min, pvs),
        MIN_DIRA(dira_min, pvs),
        MIN_BPVLTIME(lifetime_min, pvs),
    )

    return F.require_all(*combination_selection), (
        F.ALL if not vertex_selection else F.require_all(*vertex_selection))


@configurable
def make_combination(decay_descriptor,
                     inputs,
                     pvs,
                     *,
                     build_requirements=None,
                     mass_range,
                     can_reco_vertex=True,
                     name):
    """ General combiner """
    combination_selection, vertex_selection = combination_requirements(
        pvs,
        mass_range=mass_range,
        **({} if build_requirements is None else build_requirements()))
    return ParticleCombiner(
        inputs,
        ParticleCombiner=('ParticleVertexFitter'
                          if can_reco_vertex else 'ParticleAdder'),
        DecayDescriptor=decay_descriptor,
        CombinationCut=combination_selection,
        CompositeCut=vertex_selection,
        name=name,
    )


@configurable
def filter_combination(combination,
                       pvs,
                       *,
                       build_requirements=None,
                       mass_range,
                       name):
    """ Filter a combination candidate """
    combination_selection, vertex_selection = combination_requirements(
        pvs,
        mass_range=mass_range,
        **({} if build_requirements is None else build_requirements()))
    return ParticleFilter(
        combination,
        F.FILTER(F.require_all(combination_selection, vertex_selection)),
        name=name)


@configurable
def make_dielectron(particle_id,
                    pvs,
                    *,
                    mass_range,
                    dielectron_pt_min=0. * MeV,
                    brem_adder='SelectiveBremAdder',
                    opposite_sign=True,
                    electron_requirements='default'):
    """
    Create a dielectron candidate where we avoid using the
    same radiation photon more than once
    """
    dielectron_mass_min, dielectron_mass_max = mass_range

    if electron_requirements == 'default':
        electrons_builder = make_electrons
    elif electron_requirements == 'tight':
        electrons_builder = make_tight_electrons
    elif electron_requirements == 'loose':
        electrons_builder = make_loose_electrons
    else:
        raise ValueError(
            f'Unknown electron requirements "{electron_requirements}"; choose between "default", "tight" or "loose"'
        )

    return FunctionalDiElectronMaker(
        InputParticles=electrons_builder(pvs, add_brem=False),
        MinDiElecPT=dielectron_pt_min,
        MinDiElecMass=dielectron_mass_min,
        MaxDiElecMass=dielectron_mass_max,
        DiElecID=particle_id,
        OppositeSign=opposite_sign,
        BremAdder=brem_adder).Particles


def requirements_builder(function):
    """
    Wrap a function to update the configuration returned by it with the
    provided keyword arguments. This allows to keep having a configurable
    function defining a set of requirements but in such a way that we do
    not need to propagate by hand the different keyword arguments.
    """

    @configurable
    @functools.wraps(function)
    def wrapper(**kwargs):
        requirements = function()
        requirements.update(kwargs)
        return requirements

    return wrapper


@requirements_builder
def requirements_for_decay_products(**kwargs):
    """ General requirements for the decay products """
    return dict(
        p_min=3 * GeV,
        pt_min=80 * MeV,
        track_chi2dof_max=9.,
        ghostprob_max=0.2,
        ip_min=1 * mm,
        ipchi2_min=36.,
    )


@requirements_builder
def loose_requirements_for_decay_products(**kwargs):
    """ Tight requirements for decay products """
    return dict(
        p_min=3 * GeV,
        pt_min=80 * MeV,
        track_chi2dof_max=16.,
        ghostprob_max=0.3,
        ip_min=0.5 * mm,
        ipchi2_min=25.,
    )


@requirements_builder
def tight_requirements_for_decay_products(**kwargs):
    """ Tight requirements for decay products """
    return dict(
        p_min=3 * GeV,
        pt_min=80 * MeV,
        track_chi2dof_max=4.,
        ghostprob_max=0.1,
        ip_min=5. * mm,
        ipchi2_min=256.,
    )


@requirements_builder
def requirements_for_prompt_neutral_combination(**kwargs):
    """
    Requirements on the vertex for any combination involving a neutral particle
    """
    return dict(
        dira_min=0.9,
        docachi2_max=36.,
        doca_max=2 * mm,
        vchi2pdof_max=36.,
        ipchi2_max=36.,
    )


@requirements_builder
def requirements_for_combination(**kwargs):
    """
    Requirements on the vertex for any combination
    """
    return dict(
        bpvvdz_min=0. * mm,
        docachi2_max=25.,
        doca_max=0.2 * mm,
        vchi2pdof_max=25.,
        ip_bpvvdz_ratio_max=1. / 60,
    )


@requirements_builder
def requirements_for_prompt_combination(**kwargs):
    """
    Selection for a combination that is assumed to come from a
    primary vertex. We add requirements on the maximum distance
    and the minimum displacement in the perpendicular plane with
    respect to any primary vertex.
    """
    return requirements_for_combination(
        ip_max=0.4 * mm, bpvvdrho_min=3. * mm, dira_min=0.999)


@requirements_builder
def requirements_for_detached_combination(**kwargs):
    """
    Requirements for a combination that does not come from
    a primary vertex (predominantly semileptonic decays).
    """
    return requirements_for_combination(ip_min=0.4 * mm, rho_min=3. * mm)


@requirements_builder
def requirements_for_detached_combination_no_ip(**kwargs):
    """
    Requirements for a combination that does not come from
    a primary vertex (predominantly semileptonic decays). We
    simply add a requirement on the vertex position with respect
    to the Z axis. The lack of a requirement on the impact
    parameter allows to use this requirement also on the
    normalization modes.
    """
    return requirements_for_combination(rho_min=3. * mm)


@requirements_builder
def tight_requirements_for_detached_combination_no_ip(**kwargs):
    """
    Similar to "requirements_for_detached_combination_no_ip" but with
    tighter requirements
    """
    return requirements_for_detached_combination_no_ip(rho_min=5. * mm)


@requirements_builder
def requirements_for_prompt_ks0(**kwargs):
    """
    Requirements for a KS0 that comes from a PV
    """
    return requirements_for_prompt_combination(
        lifetime_min=0.0045 * ns,  # eff. ~ 95 %
    )


def _make_electrons(pvs, *, add_brem=True, **kwargs):
    """ Filter long electrons with or without Bremsstrahlung """
    function = make_long_electrons_with_brem if add_brem else make_long_electrons_no_brem
    return filter_decay_products(function(), pvs, **kwargs)


@configurable
def make_electrons(pvs, *, pid_e_min=-3, **kwargs):
    """ Filter long electrons with or without Bremsstrahlung """
    return _make_electrons(
        pvs,
        pid_e_min=pid_e_min,
        name='ElectronsForStrangeDecays_{hash}',
        **requirements_for_decay_products(**kwargs))


@configurable
def make_loose_electrons(pvs, **kwargs):
    """ Filter long electrons with or without Bremsstrahlung """
    return _make_electrons(
        pvs,
        name='LooseElectronsForStrangeDecays_{hash}',
        **loose_requirements_for_decay_products(**kwargs))


@configurable
def make_tight_electrons(pvs, *, pid_e_min=0, **kwargs):
    """ Filter long electrons with or without Bremsstrahlung """
    return _make_electrons(
        pvs,
        pid_e_min=pid_e_min,
        name='TightElectronsForStrangeDecays_{hash}',
        **tight_requirements_for_decay_products(**kwargs))


@configurable
def make_muons(pvs, *, pid_mu_min=-3, **kwargs):
    """ Filter long muons with IsMuon applied """
    return filter_decay_products(
        make_ismuon_long_muon(),
        pvs,
        pid_mu_min=pid_mu_min,
        name='MuonsForStrangeDecays_{hash}',
        **requirements_for_decay_products(**kwargs))


@configurable
def make_loose_muons(pvs, **kwargs):
    """ Filter long muons with IsMuon applied """
    return filter_decay_products(
        make_long_muons(),
        pvs,
        name='LooseMuonsForStrangeDecays_{hash}',
        **loose_requirements_for_decay_products(**kwargs))


@configurable
def make_tight_muons(pvs, pid_mu_min=0., **kwargs):
    """ Filter long muons with IsMuon applied """
    return filter_decay_products(
        make_ismuon_long_muon(),
        pvs,
        pid_mu_min=pid_mu_min,
        name='TightMuonsForStrangeDecays_{hash}',
        **tight_requirements_for_decay_products(**kwargs))


@configurable
def make_pions(pvs, **kwargs):
    """ Filter long pions """
    return filter_decay_products(
        make_long_pions(),
        pvs,
        name='PionsForStrangeDecays_{hash}',
        **requirements_for_decay_products(**kwargs))


@configurable
def make_loose_pions(pvs, **kwargs):
    """ Filter long pions """
    return filter_decay_products(
        make_long_pions(),
        pvs,
        name='LoosePionsForStrangeDecays_{hash}',
        **loose_requirements_for_decay_products(**kwargs))


@configurable
def make_tight_pions(pvs, **kwargs):
    """ Filter long pions """
    return filter_decay_products(
        make_long_pions(),
        pvs,
        name='TightPionsForStrangeDecays_{hash}',
        **tight_requirements_for_decay_products(**kwargs))


@configurable
def make_kaons(pvs, **kwargs):
    """ Filter long kaons """
    return filter_decay_products(
        make_long_kaons(),
        pvs,
        name='KaonsForStrangeDecays_{hash}',
        **requirements_for_decay_products(**kwargs))


@configurable
def make_tight_kaons(pvs, **kwargs):
    """ Filter long kaons """
    return filter_decay_products(
        make_long_kaons(),
        pvs,
        name='TightKaonsForStrangeDecays_{hash}',
        **tight_requirements_for_decay_products(**kwargs))


@configurable
def make_photons(pvs, pt_min=250 * MeV, is_not_h_min=None, is_photon_min=None):
    """ Make photons """
    particles = PhotonMaker(
        InputProtoParticles=make_neutral_protoparticles(),
        InputPrimaryVertices=pvs).Particles
    return filter_decay_products(
        particles,
        pvs,
        pt_min=pt_min,
        is_not_h_min=is_not_h_min,
        is_photon_min=is_photon_min,
        name='PhotonsForStrangeDecays_{hash}')


@configurable
def make_resolved_neutral_pions(pvs,
                                mass_range=(105 * MeV, 165 * MeV),
                                pt_min=1 * GeV):
    """ Filter resolved neutral pions """
    particles = make_photons(pvs)

    mass_min, mass_max = mass_range

    return ParticleCombiner(
        Inputs=[particles, particles],
        DecayDescriptor='pi0 -> gamma gamma',
        CombinationCut=in_range(mass_min, F.MASS, mass_max),
        CompositeCut=F.PT > pt_min,
        ParticleCombiner='ParticleAdder',
        name='ResolvedNeutralPionsForStrangeDecays_{hash}')


@configurable
def make_merged_neutral_pions(pvs, mass_window=60 * MeV, pt_min=2 * GeV):
    """ Filter merged neutral pions """
    return MergedPi0Maker(
        InputProtoParticles=make_neutral_protoparticles(),
        InputPrimaryVertices=pvs,
        MassWindow=mass_window,
        PtCut=pt_min,
        name='MergedNeutralPionsForStrangeDecays_{hash}').Particles


@configurable
def make_protons(pvs, *, pid_p_min=0, pid_p_over_k_min=0, **kwargs):
    """ Filter long protons """
    return filter_decay_products(
        make_long_protons(),
        pvs,
        pid_p_min=pid_p_min,
        pid_p_over_k_min=pid_p_over_k_min,
        name='ProtonsForStrangeDecays_{hash}',
        **requirements_for_decay_products(**kwargs))


@configurable
def make_tight_protons(pvs, *, pid_p_min=+5, pid_p_over_k_min=+3, **kwargs):
    """ Filter long protons """
    return filter_decay_products(
        make_long_protons(),
        pvs,
        pid_p_min=pid_p_min,
        pid_p_over_k_min=pid_p_over_k_min,
        name='TightProtonsForStrangeDecays_{hash}',
        **kwargs,
        **tight_requirements_for_decay_products(**kwargs))


@configurable
def ks0_mass_bounds(comb_mass_min=400 * MeV, comb_mass_max=600 * MeV):
    """ Allow to define the same mass bounds for many lines that select KS0 decays """
    return comb_mass_min, comb_mass_max


@configurable
def ks0_tight_mass_bounds(comb_mass_min=470 * MeV, comb_mass_max=600 * MeV):
    """ Allow to define the same mass bounds for many lines that select KS0 decays """
    return comb_mass_min, comb_mass_max


@configurable
def kplus_mass_bounds(comb_mass_min=400 * MeV, comb_mass_max=600 * MeV):
    """ Allow to define the same mass bounds for many lines that select K+ decays """
    return comb_mass_min, comb_mass_max


@configurable
def lambda0_mass_bounds(comb_mass_min=1000 * MeV, comb_mass_max=1200 * MeV):
    """ Allow to define the same mass bounds for many lines that select Lambda0 decays """
    return comb_mass_min, comb_mass_max


@configurable
def sigma_mass_bounds(comb_mass_min=1000 * MeV, comb_mass_max=1300 * MeV):
    """ Allow to define the same mass bounds for many lines that select Sigma+ decays """
    return comb_mass_min, comb_mass_max


@configurable
def xi_mass_bounds(comb_mass_min=1200 * MeV, comb_mass_max=1400 * MeV):
    """ Allow to define the same mass bounds for many lines that select Xi-/0 decays """
    return comb_mass_min, comb_mass_max


@configurable
def omega_mass_bounds(comb_mass_min=1500 * MeV, comb_mass_max=1800 * MeV):
    """ Allow to define the same mass bounds for many lines that select Omega- decays """
    return comb_mass_min, comb_mass_max


@configurable
def build_lambda0(pvs):
    """ Build Lambda0 baryons """
    proton = make_protons(pvs)
    pion = make_pions(pvs)
    return make_combination(
        '[Lambda0 -> p+ pi-]cc', [proton, pion],
        pvs,
        build_requirements=requirements_for_detached_combination_no_ip,
        mass_range=lambda0_mass_bounds(),
        name='Lambda0ForStrangeDecays_Combiner_{hash}')


@configurable
def build_tight_lambda0(pvs):
    """ Build Lambda0 baryons """
    proton = make_tight_protons(pvs)
    pion = make_tight_pions(pvs)
    return make_combination(
        '[Lambda0 -> p+ pi-]cc', [proton, pion],
        pvs,
        build_requirements=requirements_for_detached_combination_no_ip,
        mass_range=lambda0_mass_bounds(),
        name='TightLambda0ForStrangeDecays_Combiner_{hash}')


@configurable
def build_dimuon_intermediate_neutral(particle_name, pvs, *, mass_max):
    """ Build a neutral particle from two muons with loose requirements """
    muons = make_loose_muons(pvs)
    return make_combination(
        f'{particle_name} -> mu+ mu-', [muons, muons],
        pvs,
        build_requirements=requirements_for_detached_combination,
        mass_range=(dimuon_min_mass(), mass_max),
        name='DiMuon_IntermediateNeutral_Combiner_{hash}')


@configurable
def build_dielectron_intermediate_neutral(particle_name, pvs, *, mass_max):
    """ Build a neutral particle from two electrons with loose requirements """
    mass_range = (dielectron_min_mass(), mass_max)
    return filter_combination(
        make_dielectron(
            particle_name,
            pvs,
            mass_range=mass_range,
            electron_requirements='loose'),
        pvs,
        build_requirements=requirements_for_detached_combination,
        mass_range=mass_range,
        name='DiElectron_IntermediateNeutral_Filter_{hash}')


@configurable
def dielectron_min_mass(mass_min=20. * MeV):
    """ Requirement to avoid a peak at low mass in the dielectron spectrum """
    return mass_min


@configurable
def dimuon_min_mass(mass_min=220. * MeV):
    """ Requirement to avoid a peak at low mass in the dimuon spectrum """
    return mass_min


@configurable
def pion_muon_min_mass(mass_min=350. * MeV):
    """ Requirement to avoid KS0 -> pi+ pi- candidates in the pion-muon spectrum """
    return mass_min


@configurable
def pion_electron_min_mass(mass_min=250. * MeV):
    """ Requirement to avoid KS0 -> pi+ pi- candidates in the pion-electron spectrum """
    return mass_min


@configurable
def muon_electron_min_mass(mass_min=105. * MeV):
    """ Requirement for [mu+, e-]cc combinations """
    return mass_min
