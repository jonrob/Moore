###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""                                                                                               
Author: Raja Nandakumar, Fergus Wilson
Contact: raja.nandakumar@cern.ch
Date: 29/03/2022

Extra builders :
make_prompt_tautau : For Upsilon(1S) -> tau+ tau-
    - Invariant mass : [7500, 13000] MeV
    - Cos(opening angle) > 0.95
    - PT > 1.5 GeV
    - Tau cuts :
      - Invariant MAss : [1200, 2000] MeV
      - PID_K < -1.0

make_prompt_etau : For Upsilon(1S) -> e+ tau-
    - Invariant mass : [8000, 12000] MeV
    - Cos(opening angle) > 0.95
    - PT > 2.5 GeV
    - Tau cuts :
      - Invariant MAss : [1200, 2000] MeV
      - PID_K < -1.0
    - electron cuts:
      - PID_E > 2.5
      - PT > 1.5 GeV

make_prompt_mutau: For Upsilon(1S) -> mu+ tau-
    - Invariant mass : [7000, 13000] MeV
    - PT > 1 GeV
    - Tau cuts :
      - Invariant MAss : [1000, 2000] MeV
      - PID_K < 0.0
    - muon cuts:
      - PID_MU > 2.0

make_upsilons_to_upsilons : For Upsilon(2S) -> Upsilon(1S) pi+ pi-
    - Invariant mass : [7000, 13000] MeV
    - PT > 1 GeV
    - Pion cuts :
      - PID_K < 0.0
      - PT > 250 MeV
"""

from __future__ import absolute_import, division, print_function
from PyConf import configurable
from GaudiKernel.SystemOfUnits import GeV, MeV
from RecoConf.reconstruction_objects import make_pvs

from Hlt2Conf.lines.rd.builders.rdbuilder_thor import (
    make_rd_prompt_muons,
    make_rd_detached_pions,
)
from Hlt2Conf.lines.rd.builders.rdbuilder_thor import (
    make_rd_tauons_hadronic_decay,
    make_rd_prompt_electrons,
)
from Hlt2Conf.algorithms_thor import ParticleCombiner

import Functors as F
from Functors.math import in_range


@configurable
def make_prompt_tautau(
        name="prompt_tautau_builder_{hash}",
        min_dilepton_mass=7500.0 * MeV,
        max_dilepton_mass=13000.0 * MeV,
        min_dilepton_pt=1.5 * GeV,
        max_ipchi2_tautau=10,
        max_adocachi2=30.0,
        parent_id="Upsilon(1S)",
        min_bpvvdchi2=0.0,  # must be 0 for a prompt builder
        cos_tautauangle=0.95,
):
    pvs = make_pvs()
    descriptor = "{} -> tau+ tau-".format(parent_id)
    taus = make_rd_tauons_hadronic_decay(
        pi_pt_min=300 * MeV,
        pi_ipchi2_min=3.0,
        best_pi_pt_min=800 * MeV,
        best_pi_ipchi2_min=5.0,
        pi_pid=F.PID_K < -1.0,
        am_min=1200 * MeV,
        am_max=2000 * MeV,
        vchi2pdof_max=12.0,
    )

    combination_code = F.require_all(
        F.MAXDOCACHI2CUT(max_adocachi2),
        F.ALV(1, 2) > cos_tautauangle,
    )
    vertex_code = F.require_all(
        F.BPVFDCHI2(pvs) > min_bpvvdchi2,
        in_range(min_dilepton_mass, F.MASS, max_dilepton_mass),
        F.PT > min_dilepton_pt,
        F.BPVIPCHI2(pvs) < max_ipchi2_tautau,
    )
    tautau = ParticleCombiner(
        name=name,
        Inputs=[taus, taus],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )
    return tautau


#####


@configurable
def make_prompt_etau(
        name="prompt_etau_builder",
        min_dilepton_mass=8000.0 * MeV,
        max_dilepton_mass=12000.0 * MeV,
        min_dilepton_pt=2.5 * GeV,
        max_ipchi2_etau=9,
        parent_id="Upsilon(1S)",
        min_pid_e=2.5,
        min_pt_e=1.5 * GeV,
        max_adocachi2=20.0,
        min_bpvvdchi2=0.0,  # must be 0 for a prompt builder
        cos_etauangle=0.95,
):
    """
    Make the detached electron-tau pair, opposite-sign for now.
    """
    pvs = make_pvs()
    descriptor = "[{} -> tau- e+]cc".format(parent_id)
    # taus = make_upsilon_taus()
    taus = make_rd_tauons_hadronic_decay(
        pi_pt_min=350 * MeV,
        pi_ipchi2_min=3.0,
        best_pi_pt_min=800 * MeV,
        best_pi_ipchi2_min=5.0,
        pi_pid=F.PID_K < -1.0,
        am_min=1200 * MeV,
        am_max=2000 * MeV,
        vchi2pdof_max=12.0,
    )
    electrons = make_rd_prompt_electrons(
        pt_min=min_pt_e, pid=(F.PID_E > min_pid_e))
    combination_code = F.require_all(
        F.MAXDOCACHI2CUT(max_adocachi2),
        F.ALV(1, 2) > cos_etauangle,
    )
    vertex_code = F.require_all(
        F.BPVFDCHI2(pvs) > min_bpvvdchi2,
        in_range(min_dilepton_mass, F.MASS, max_dilepton_mass),
        F.PT > min_dilepton_pt,
        F.BPVIPCHI2(pvs) < max_ipchi2_etau,
    )
    etau = ParticleCombiner(
        name=name,
        Inputs=[taus, electrons],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )
    return etau


#####


@configurable
def make_prompt_mutau(
        name="prompt_mutau_builder",
        min_dilepton_mass=7000.0 * MeV,
        max_dilepton_mass=13000.0 * MeV,
        min_dilepton_pt=1.0 * GeV,
        max_ipchi2_mutau=50,
        parent_id="Upsilon(1S)",
        min_PIDmu=2.0,
        IsMuon=True,
        min_pt_mu=0.0 * GeV,
        max_adocachi2=30.0,
        min_bpvvdchi2=0.0,  # must be 0 for a prompt builder
):
    """
    Make the detached muon-tau pair, opposite-sign for now.
    """
    pvs = make_pvs()
    descriptor = "[{} -> mu+ tau-]cc".format(parent_id)
    taus = make_rd_tauons_hadronic_decay(
        pi_pt_min=250 * MeV,
        pi_ipchi2_min=6.0,
        best_pi_pt_min=800 * MeV,
        best_pi_ipchi2_min=9.0,
        pi_pid=F.PID_K < 0.0,
        am_min=1000 * MeV,
        am_max=2000 * MeV,
        vchi2pdof_max=16.0,
    )

    if IsMuon:
        muons = make_rd_prompt_muons(
            pt_min=min_pt_mu,
            pid=F.require_all(F.PID_MU > min_PIDmu, F.ISMUON))
    else:
        muons = make_rd_prompt_muons(
            pt_min=min_pt_mu, pid=(F.PID_MU > min_PIDmu))
    combination_code = F.MAXDOCACHI2CUT(max_adocachi2)
    vertex_code = F.require_all(
        F.BPVFDCHI2(pvs) > min_bpvvdchi2,
        in_range(min_dilepton_mass, F.MASS, max_dilepton_mass),
        F.PT > min_dilepton_pt,
        F.BPVIPCHI2(pvs) < max_ipchi2_mutau,
    )
    mutau = ParticleCombiner(
        name=name,
        Inputs=[muons, taus],
        DecayDescriptor=descriptor,
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )
    return mutau


#####
@configurable
def make_upsilons_to_upsilons(
        upsilon1s,
        name="upsilon_ll_builder_{hash}",
        min_dilepton_mass=7000.0 * MeV,
        max_dilepton_mass=13000.0 * MeV,
        min_dilepton_pt=1.0 * GeV,
        min_bpvvdchi2=0.0,  # must be 0 for a prompt builder
        max_adocachi2=30.0,
        minipchi2=0,  # must be 0 for a prompt builder
        # pion cut
        pi_pt_min=250 * MeV,
        pi_ipchi2_min=0.0,  # check if you want to filter on MINIPCHI2(pvs) < 0. or MINIPCHI2(pvs) < 1.
        pi_pid=F.PID_K < 0.0,
):
    pion = make_rd_detached_pions(
        pt_min=pi_pt_min, mipchi2dvprimary_min=pi_ipchi2_min, pid=pi_pid)

    pvs = make_pvs()
    combination_code = F.require_all(
        in_range(min_dilepton_mass, F.MASS, max_dilepton_mass),
        F.MAXDOCACHI2CUT(max_adocachi2),
    )

    vertex_code = F.require_all(
        in_range(min_dilepton_mass, F.MASS, max_dilepton_mass),
        F.PT > min_dilepton_pt,
        F.BPVFDCHI2(pvs) > min_bpvvdchi2,
    )

    upsilon2s = ParticleCombiner(
        [upsilon1s, pion, pion],
        name=name,
        DecayDescriptor="[Upsilon(2S) -> Upsilon(1S) pi+ pi-]cc",
        CombinationCut=combination_code,
        CompositeCut=vertex_code,
    )
    return upsilon2s
