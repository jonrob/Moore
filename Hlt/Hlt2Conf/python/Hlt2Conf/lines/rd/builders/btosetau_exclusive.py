###############################################################################
# (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from GaudiKernel.SystemOfUnits import MeV, mm
import Functors as F

from Hlt2Conf.algorithms_thor import ParticleCombiner


def make_phie(kaons1,
              kaons2,
              electrons,
              pvs,
              comb_m_max=3600 * MeV,
              vchi2pdof_max=8,
              docachi2_max=8.0,
              doca_max=0.8 * mm,
              dira_min=0.5,
              phi_docachi2_max=4.0,
              phi_doca_max=0.15 * mm,
              decay_descriptor="[B0 -> K+ K- e+]cc",
              name="rd_dikaon_electron_for_btosetau_{hash}"):
    """Builder for X -> K K e decays, used in XETau lines"""
    combination_code = F.require_all(F.MASS < comb_m_max,
                                     F.MAXSDOCACHI2CUT(docachi2_max),
                                     F.MAXSDOCACUT(doca_max))
    two_body_combination_code = F.require_all(
        F.MASS < comb_m_max, F.MAXSDOCACHI2CUT(phi_docachi2_max),
        F.MAXSDOCACUT(phi_doca_max))
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVDIRA(pvs) > dira_min)
    phie = ParticleCombiner([kaons1, kaons2, electrons],
                            DecayDescriptor=decay_descriptor,
                            Combination12Cut=two_body_combination_code,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code,
                            name=name)
    return phie


def make_ke(kaons,
            electrons,
            pvs,
            comb_m_max=3550 * MeV,
            vchi2pdof_max=4,
            docachi2_max=4.0,
            doca_max=0.6 * mm,
            dira_min=0.5,
            decay_descriptor="[B0 -> e+ K-]cc",
            name="rd_kaon_electron_for_btosetau_{hash}"):
    """Builder for X -> K e decays, used in XETau lines"""
    combination_code = F.require_all(F.MASS < comb_m_max,
                                     F.MAXSDOCACHI2CUT(docachi2_max),
                                     F.MAXSDOCACUT(doca_max))
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVDIRA(pvs) > dira_min)
    ke = ParticleCombiner([electrons, kaons],
                          DecayDescriptor=decay_descriptor,
                          CombinationCut=combination_code,
                          CompositeCut=vertex_code,
                          name=name)
    return ke


def make_kste(kaons,
              pions,
              electrons,
              pvs,
              comb_m_max=3550 * MeV,
              vchi2pdof_max=6,
              docachi2_max=6.0,
              doca_max=0.6 * mm,
              dira_min=0.5,
              kst_docachi2_max=3.0,
              kst_doca_max=0.12 * mm,
              decay_descriptor="[B0 -> K+ pi- e+]cc",
              name="rd_kpi_electron_for_btosetau_{hash}"):
    """Builder for X -> K pi e decays, used in XETau lines"""
    combination_code = F.require_all(F.MASS < comb_m_max,
                                     F.MAXSDOCACHI2CUT(docachi2_max),
                                     F.MAXSDOCACUT(doca_max))
    two_body_combination_code = F.require_all(
        F.MASS < comb_m_max, F.MAXSDOCACHI2CUT(kst_docachi2_max),
        F.MAXSDOCACUT(kst_doca_max))
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVDIRA(pvs) > dira_min)
    kste = ParticleCombiner([kaons, pions, electrons],
                            DecayDescriptor=decay_descriptor,
                            Combination12Cut=two_body_combination_code,
                            CombinationCut=combination_code,
                            CompositeCut=vertex_code,
                            name=name)
    return kste


def make_pke(kaons,
             protons,
             electrons,
             pvs,
             comb_m_max=4000 * MeV,
             vchi2pdof_max=8,
             docachi2_max=8.0,
             doca_max=0.8 * mm,
             dira_min=0.5,
             pk_docachi2_max=4.0,
             pk_doca_max=0.15 * mm,
             decay_descriptor="[B0 -> p+ K- e+]cc",
             name="rd_pk_electron_for_btosetau_{hash}"):
    """Builder for X -> p K e decays, used in XETau lines"""
    combination_code = F.require_all(F.MASS < comb_m_max,
                                     F.MAXSDOCACHI2CUT(docachi2_max),
                                     F.MAXSDOCACUT(doca_max))
    two_body_combination_code = F.require_all(
        F.MASS < comb_m_max, F.MAXSDOCACHI2CUT(pk_docachi2_max),
        F.MAXSDOCACUT(pk_doca_max))
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVDIRA(pvs) > dira_min)
    pke = ParticleCombiner([protons, kaons, electrons],
                           DecayDescriptor=decay_descriptor,
                           Combination12Cut=two_body_combination_code,
                           CombinationCut=combination_code,
                           CompositeCut=vertex_code,
                           name=name)
    return pke


def make_bs(kkes,
            electrons,
            pvs,
            comb_m_max=7000 * MeV,
            vchi2pdof_max=75,
            dira_min=0.999,
            bpvfdchi2_min=50,
            decay_descriptor="[B_s0 -> B0 e-]cc",
            name="rd_make_bs_to_kktaue_{hash}"):
    """Builder for Bs -> X e tau(-> e nu nu) decays, where X is K K"""
    combination_code = F.MASS < comb_m_max
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVDIRA(pvs) > dira_min,
                                F.BPVFDCHI2(pvs) > bpvfdchi2_min)
    bs = ParticleCombiner([kkes, electrons],
                          DecayDescriptor=decay_descriptor,
                          CombinationCut=combination_code,
                          CompositeCut=vertex_code,
                          name=name)
    return bs


def make_bu(kes,
            electrons,
            pvs,
            comb_m_max=6750 * MeV,
            vchi2pdof_max=25,
            dira_min=0.999,
            bpvfdchi2_min=50,
            decay_descriptor="[B_s0 -> B0 e-]cc",
            name="rd_make_bu_to_ketau_{hash}"):
    """Builder for Bs -> X e tau(-> e nu nu) decays, where X is K"""
    combination_code = F.MASS < comb_m_max
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVDIRA(pvs) > dira_min,
                                F.BPVFDCHI2(pvs) > bpvfdchi2_min)
    bu = ParticleCombiner([kes, electrons],
                          DecayDescriptor=decay_descriptor,
                          CombinationCut=combination_code,
                          CompositeCut=vertex_code,
                          name=name)
    return bu


def make_lb(pkes,
            electrons,
            pvs,
            comb_m_max=7250 * MeV,
            vchi2pdof_max=50,
            dira_min=0.999,
            bpvfdchi2_min=50,
            decay_descriptor="[Lambda_b0 -> B0 e-]cc",
            name="rd_make_lb_to_pketau_{hash}"):
    """Builder for Lb -> X e tau(-> e nu nu) decays, where X is p K"""
    combination_code = F.MASS < comb_m_max
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVDIRA(pvs) > dira_min,
                                F.BPVFDCHI2(pvs) > bpvfdchi2_min)
    lb = ParticleCombiner([pkes, electrons],
                          DecayDescriptor=decay_descriptor,
                          CombinationCut=combination_code,
                          CompositeCut=vertex_code,
                          name=name)
    return lb


def make_bd(kstes,
            electrons,
            pvs,
            comb_m_max=6750 * MeV,
            vchi2pdof_max=50,
            dira_min=0.999,
            bpvfdchi2_min=50,
            decay_descriptor="[B_s0 -> B0 e+]cc",
            name="rd_make_bd_to_kpietau_{hash}"):
    """Builder for Bd -> X e tau(-> e nu nu) decays, where X is K pi"""
    combination_code = F.MASS < comb_m_max
    vertex_code = F.require_all(F.CHI2DOF < vchi2pdof_max,
                                F.BPVDIRA(pvs) > dira_min,
                                F.BPVFDCHI2(pvs) > bpvfdchi2_min)
    bd = ParticleCombiner([kstes, electrons],
                          DecayDescriptor=decay_descriptor,
                          CombinationCut=combination_code,
                          CompositeCut=vertex_code,
                          name=name)
    return bd
