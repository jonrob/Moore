###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options
from RecoConf.decoders import default_VeloCluster_source

# Use velo retina decoding:
default_VeloCluster_source.global_bind(bank_type="VPRetinaCluster")

# To be removed once data is updated to use a tag including
# https://gitlab.cern.ch/lhcb-conddb/DDDB/-/merge_requests/107
#from DDDB.CheckDD4Hep import UseDD4Hep
#if UseDD4Hep:
#    from Configurables import DDDBConf
#    DDDBConf().GeometryVersion = 'run3/before-rich1-geom-update-26052022'

options.set_input_and_conds_from_testfiledb(
    'upgrade_Sept2022_minbias_0fb_md_mdf')

# Override settings used by TestFileDB to set geometry version correctly
# To be removed once
#    https://gitlab.cern.ch/lhcb-datapkg/PRConfig/-/merge_requests/334
# is merged and in a released tag.
from DDDB.CheckDD4Hep import UseDD4Hep
if UseDD4Hep:
    from Configurables import DDDBConf
    DDDBConf().GeometryVersion = 'run3/before-rich1-geom-update-26052022'

options.input_files = list(set(options.input_files))  # remove dups
options.persistreco_version = 0.0
options.evt_max = 300

options.use_iosvc = True
options.event_store = 'EvtStoreSvc'
