###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Logic for cloning HLT2 line outputs under ``/Event/HLT2(Spruce/)``.

This is largely a port of the Run 2 logic, up until the serialisation of the
packed containers into raw banks. 

The Run 2 logic is defined in the Hlt project. Reading the `_persistRecoSeq`
method in the `HltAfterburner.py` file is a good place to start if you want to
understand more.
"""

from PyConf.Algorithms import CopySignalMCParticles, CopyProtoParticle2MCRelations

from PyConf.location_prefix import prefix
from PyConf.reading import get_mc_particles


def mc_cloners(stream, protoparticle_relations):
    """Copy signal _and_ associated MC particles to microDST-like locations.

    In the Stripping, the microDST machinery only saves a subset of the
    input MC particles and vertices. This subset contains two possibly
    overlapping contributions:

    1. The MCParticle objects in the signal decay tree; and
    2. The MCParticle objects which can be associated to the reconstructed
    objects selected by at least one stripping line.

    To mimic this behaviour in Tesla, we run almost the same set of
    microDST algorithms, using the relations tables Tesla makes to find the
    MC particles that have been associated to the reconstruction.

    Args:
        stream (str): TES root under which objects will be cloned.
        protoparticle_relations (list of DataHandle): ProtoParticle-to-MCParticle relations.
    """

    # Algorithm to clone the existing relations tables and the MC particles
    # and vertices these tables point to from their original location to
    # the same location prefixed by /Event/Turbo
    copy_pp2mcp = CopyProtoParticle2MCRelations(
        # name = 'Copy_'+protoparticle_relations.replace('/','_'),
        InputLocations=protoparticle_relations,
        OutputPrefix=stream,
        OutputLevel=4)

    # Algorithm to clone all MC particles and vertices that are associated
    # to the simulated signal process (using LHCb::MCParticle::fromSignal)
    if "Spruce" in stream:
        MCParticlesLocation = "/Event/HLT2/MC/Particles"
        ExtraOutputs = [
            prefix(l, stream)
            for l in ['HLT2/MC/Particles', 'HLT2/MC/Vertices']
        ]
        mc_location_mapping = {
            "/Event/HLT2/MC/Particles": ExtraOutputs[0],
            "/Event/HLT2/MC/Vertices": ExtraOutputs[1]
        }
    else:
        MCParticlesLocation = "/Event/MC/Particles"
        ExtraOutputs = [
            prefix(l, stream) for l in ['MC/Particles', 'MC/Vertices']
        ]
        mc_location_mapping = {
            "/Event/MC/Particles": ExtraOutputs[0],
            "/Event/MC/Vertices": ExtraOutputs[1]
        }

    #TODO  must add those MC particles which appear in the 'to' side of the protoparticle_relations!!!
    #      and then remove the need for CopytProtoParticle2MCRelations...
    copy_signal_mcp = CopySignalMCParticles(
        # name='Copy_' + MCParticlesLocation.replace('/', '_'),
        MCParticlesLocation=get_mc_particles(MCParticlesLocation),
        OutputPrefix=stream,
        ExtraOutputs=ExtraOutputs)

    return ((copy_pp2mcp, copy_signal_mcp), mc_location_mapping)
