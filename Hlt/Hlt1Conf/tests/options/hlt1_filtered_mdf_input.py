###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Moore import options

options.dddb_tag = "dddb-20180815"
options.conddb_tag = "sim-20180530-vc-md100"
options.input_files = ['test_allen_hlt1_persistence_mdf_write.mdf']
options.input_type = "MDF"
options.evt_max = 1000
options.simulation = True
