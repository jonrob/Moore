<?xml version="1.0" encoding="UTF-8"?><!DOCTYPE extension  PUBLIC '-//QM/2.3/Extension//EN'  'http://www.codesourcery.com/qm/dtds/2.3/-//qm/2.3/extension//en.dtd'>
<!--
    (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration

    This software is distributed under the terms of the GNU General Public
    Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".

    In applying this licence, CERN does not waive the privileges and immunities
    granted to it by virtue of its status as an Intergovernmental Organization
    or submit itself to any jurisdiction.
-->
<!--
Test that the SelReports written out by the Track MVA test filter the same
input data file in the same way.

This is done by reading the file created by the mdf_write test with a
near-identical HLT1 configuration. The only modification is that line
decision algorithms are replaced by TOS filters that require the selected
objects are TOS with respect to the object in the SelReports (created in the
mdf_write job).
-->
<extension class="GaudiTest.GaudiExeTest" kind="test">
<argument name="prerequisites"><set>
  <tuple><text>persistency.mdf_write</text><enumeral>PASS</enumeral></tuple>
</set></argument>
<argument name="program"><text>gaudirun.py</text></argument>
<argument name="args"><set>
  <text>$MOOREROOT/tests/options/default_input_and_conds_hlt1.py</text>
  <text>$HLT1CONFROOT/tests/options/hlt1_mdf_input.py</text>
  <text>$MOOREROOT/options/muon_geometry_v2.py</text>
  <text>$HLT1CONFROOT/options/hlt1_selreports_filtering.py</text>
</set></argument>
<argument name="use_temp_dir"><enumeral>true</enumeral></argument>
<argument name="validator"><text>

from Moore.qmtest.exclusions import remove_known_warnings

# the input file creation job, so all is fine
countErrorLines({"FATAL": 0, "ERROR": 0, "WARNING": 0},
                stdout=remove_known_warnings(stdout))

import re

# Check the decision of each line and of moore as a whole
# (need to escape "less than" for xml parsing to not match "LineNameDecisionWithOutput" or "LineNameOutput" with rexex (?&lt;!Output))
pattern = re.compile(r'\s+(?:NONLAZY|LAZY)_(?:AND|OR): (?:Hlt1[A-Za-z0-9_]+|moore|hlt_decision|report_writers)(?&lt;!Output)\s+#=\d+\s+Sum=(\d+)')

# Capture positive decision counts of all control flow nodes
positive_decision_counts = []
for line in stdout.split('\n'):
    m = re.match(pattern, line)
    if m:
        nselected, = map(int, m.groups())
        positive_decision_counts.append(nselected)

# Capture the same as above, but observed in the job that created the input file
positive_decision_counts_writing = []
with open('test_hlt1_persistence_mdf_write.stdout') as f_ref:
    for line in f_ref.readlines():
        m = re.match(pattern, line)
        if m:
            nselected_writing, = map(int, m.groups())
            positive_decision_counts_writing.append(nselected_writing)

# Expect to find some decision counts
if not (positive_decision_counts and positive_decision_counts_writing):
    causes.append('decision counters not found')
# Expect that those counts are identical, both in order and values
if positive_decision_counts != positive_decision_counts_writing:
    causes.append('decision counts mismatch:\n{}\n{}'.format(
        positive_decision_counts, positive_decision_counts_writing
    ))

</text></argument>
</extension>
